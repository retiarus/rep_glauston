import numpy as np
import matplotlib.pyplot as plt

from functools import partial

from _resample import _generate_subband


def _get_pdf(X, step_outer, shift):
    space = _generate_subband(X, step_outer)

    # mean_idx store the index to the mean value
    # mean_shift will store the value associated
    # with the position mean_idx shiffted by shift variable
    mean_idx = step_outer//2
    mean = space[mean_idx]
    mean_shift = space[mean_idx + shift]

    std = np.std(X)
    cv = std/mean

    # generate differents values to coeficient of variation
    # by using the expression y=x(mean_shift/x)^r
    # the new coefficient will be cv_i = \sigma(y)/\mu(y)
    r_values = np.arange(0.1, 2, 0.001)
    cv_array = np.empty_like(r_values)
    for idx, r in enumerate(r_values):
        y = X*np.power((mean_shift/X), r)
        cv_array[idx] = np.std(y)/np.mean(y)

    vdif = np.abs(cv_array - cv/3)
    if shift < 0:
        position = np.argmin(vdif[0:950])
    elif shift > 0:
        position = np.argmin(vdif[950:1900]) + 950

    # print("Valor de r: ", r_values[position])
    y_new = X*np.power((mean_shift/X), r_values[position])
    mean_new = np.mean(y_new)
    std_new = np.std(y_new)

    # print(mean_new, std_new)
    def gaussian(x, mu, sig):
        return (1/(sig*np.sqrt(2*np.pi))) \
            * np.exp(-np.power(x - mu, 2.) / (2 * np.power(sig, 2.)))

    function = partial(gaussian, mu=mean_shift, sig=std_new)
    print(function(0.5))

    return function

def _generate_pdfs(X, y, step_outer, shift, target):
    X_0 = X[y == 0]
    X_1 = X[y == 1]

    num_columns = X.shape[1]

    # a gaussin distribution function will be generated
    # for each element in predictors and for each class,
    # considering a two classes problem

    # class 0
    array_0 = np.empty(num_columns, dtype=object)
    for i in np.arange(num_columns):
        array_0[i] = _get_pdf(X_0[:, i], step_outer, -np.abs(shift))

    # class 1
    array_1 = np.empty(num_columns, dtype=object)
    for i in np.arange(num_columns):
        array_1[i] = _get_pdf(X_1[:, i], step_outer, np.abs(shift))

    return array_0, array_1


def generate_pdf_representation(X, y, step_outer, shift, target):
    array_0, array_1 = _generate_pdfs(X, y, step_outer, shift, target)

    X_aux_0 = np.empty_like(X)
    for idx, pdf in enumerate(array_0):
        X_aux_0[:, idx] = pdf(X[:, idx])

    X_aux_1 = np.empty_like(X)
    for idx, pdf in enumerate(array_1):
        X_aux_1[:, idx] = pdf(X[:, idx])

    return np.concatenate([X_aux_0, X_aux_1], axis=1)


class ToPdfRepresentation():
    def __init__(self, num_intervals=16, shift=1, target=None):
        self._num_intervals = num_intervals
        self._shift = shift
        self._target = target

    def fit(self, X, y):
        self._array_0, self._array_1 = _generate_pdfs(X, y,
                                                      self._num_intervals,
                                                      self._shift,
                                                      self._target)

        X_aux_0, X_aux_1 = self._partial_transform(X)
        self._max_rep_1_inst_1 = np.zeros(X.shape[-1], dtype=np.float32)
        self._max_rep_0_inst_0 = np.zeros(X.shape[-1], dtype=np.float32)
        for i in np.arange(X.shape[-1]):
            self._max_rep_1_inst_1[i] = np.max(X_aux_1[y == 1][:, i])
            self._max_rep_0_inst_0[i] = np.max(X_aux_0[y == 0][:, i])

        return self._max_rep_0_inst_0, self._max_rep_1_inst_1

    def _partial_transform(self, X):
        X_aux_0 = np.empty_like(X)
        for idx, pdf in enumerate(self._array_0):
            X_aux_0[:, idx] = pdf(X[:, idx])

        X_aux_1 = np.empty_like(X)
        for idx, pdf in enumerate(self._array_1):
            X_aux_1[:, idx] = pdf(X[:, idx])

        return X_aux_0, X_aux_1

    def transform(self, X):
        X_aux_0, X_aux_1 = self._partial_transform(X)

        for i in np.arange(X_aux_0.shape[-1]):
            X_aux_0[:, i] /= self._max_rep_0_inst_0[i]
        for i in np.arange(X_aux_1.shape[-1]):
            X_aux_1[:, i] /= self._max_rep_1_inst_1[i]

        return np.concatenate([X_aux_0, X_aux_1], axis=1)


    def fit_transform(self, X, y):
        self.fit(X, y)
        return self.transform(X)
