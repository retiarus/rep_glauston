# -*- coding: utf-8 -*-
# ---
# jupyter:
#   jupytext:
#     formats: ipynb,py:percent
#     text_representation:
#       extension: .py
#       format_name: percent
#       format_version: '1.2'
#       jupytext_version: 1.0.5
#   kernelspec:
#     display_name: Python (dscience)
#     language: python
#     name: dscience
# ---

# %%
import os
import sys
import warnings
import pickle

import pdb

import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
import seaborn as sns

from evolutionary_search import EvolutionaryAlgorithmSearchCV

from keras.models import Sequential
from keras.layers import Dense

from sklearn.model_selection import train_test_split
from sklearn.metrics import recall_score, make_scorer, confusion_matrix
from sklearn.preprocessing import StandardScaler, MinMaxScaler
from sklearn.pipeline import Pipeline
from sklearn.utils.multiclass import unique_labels
from sklearn.metrics import roc_auc_score
from sklearn.tree import DecisionTreeClassifier

from xgboost import XGBClassifier
from hyperopt import STATUS_OK, Trials, fmin, hp, tpe

from functools import partial

from tests import train_test_split_intime, train_test_split_kfold
from tests import train_test_split_intime_smote, train_test_split_kfold_smote
from tests import train_test_split_intime_resample, train_test_split_kfold_resample
from tests import train_test_split_intime_adasyn, train_test_split_kfold_adasyn

from tests import test_intime, test_kfold
from tests import test_pdf_representation, test_intime_pdf_representation

# #%matplotlib inline
warnings.filterwarnings('ignore')

latter_size = 14
plt.rcParams['legend.fontsize'] = latter_size 
plt.rcParams['font.size'] = latter_size 
plt.rcParams['axes.labelsize'] = latter_size
plt.rcParams['xtick.labelsize'] = latter_size
plt.rcParams['ytick.labelsize'] = latter_size

# %%
df_train = pd.read_pickle('data_train.pkl')
df_test = pd.read_pickle('data_test.pkl')

# %%
target = ['discretize_s4', ]
predictors = [i for i in df_train.columns if i not in target]
score_function = roc_auc_score

# %%
from neuralnet import SimpleMLPGlauston2015

# %% [markdown]
# # Sem Oversamplig

# %% [markdown]
# ### Teste básico com validação cruzada

# %%
# simple Kfold cross validation
mlp = SimpleMLPGlauston2015(batch_size=20,
                            n_epoches=2000,
                            learning_rate=0.01)

test_kfold(df_train, 
           df_test, 
           train_test_split_kfold, 
           mlp, score_function, 
           predictors, 
           target)

# %% [markdown]
# ### Teste básico com separação no tempo

# %%
mlp = SimpleMLPGlauston2015(batch_size=20,
                            n_epoches=2000,
                            learning_rate=0.01)

test_intime(df_train,
            df_test,
            train_test_split_intime,
            mlp,
            score_function,
            predictors,
            target)

# %% [markdown]
# # Oversampling Glauston

# %% [markdown]
# ### Teste básico com validação cruzada

# %%
mlp = SimpleMLPGlauston2015(batch_size=20,
                            n_epoches=2000,
                            learning_rate=0.01)

test_kfold(df_train, 
           df_test, 
           train_test_split_kfold_resample, 
           mlp, score_function, 
           predictors, 
           target)

# %% [markdown]
# ### Teste básico com separação no tempo

# %%
mlp = SimpleMLPGlauston2015(batch_size=20,
                            n_epoches=2000,
                            learning_rate=0.01)

test_intime(df_train,
            df_test,
            train_test_split_intime_resample,
            mlp,
            score_function,
            predictors,
            target)

# %% [markdown]
# # Oversampling Smote

# %% [markdown]
# ### Teste básico com validação cruzada

# %%
mlp = SimpleMLPGlauston2015(batch_size=20,
                            n_epoches=2000,
                            learning_rate=0.01)

test_kfold(df_train, 
           df_test, 
           train_test_split_kfold_smote,
           mlp, score_function, 
           predictors, 
           target)

# %% [markdown]
# ### Teste básico com separação no tempo

# %%
mlp = SimpleMLPGlauston2015(batch_size=20,
                            n_epoches=2000,
                            learning_rate=0.01)

test_intime(df_train,
            df_test,
            train_test_split_intime_smote,
            mlp,
            score_function,
            predictors,
            target)

# %% [markdown]
# # Oversampling Adasyn

# %% [markdown]
# ### Teste básico com validação cruzada

# %%
mlp = SimpleMLPGlauston2015(batch_size=20,
                            n_epoches=2000,
                            learning_rate=0.01)

test_kfold(df_train, 
           df_test, 
           train_test_split_kfold_adasyn, 
           mlp, score_function, 
           predictors, 
           target)

# %% [markdown]
# ### Teste básico com separação no tempo

# %%
mlp = SimpleMLPGlauston2015(batch_size=20,
                            n_epoches=2000,
                            learning_rate=0.01)
test_intime(df_train,
            df_test,
            train_test_split_intime_adasyn,
            mlp,
            score_function,
            predictors,
            target)

# %%
