# -*- coding: utf-8 -*-
# ---
# jupyter:
#   jupytext:
#     formats: ipynb,py:percent
#     text_representation:
#       extension: .py
#       format_name: percent
#       format_version: '1.2'
#       jupytext_version: 1.0.5
#   kernelspec:
#     display_name: Python (dscience)
#     language: python
#     name: dscience
# ---

# %%
import sys
import numpy as np
import pandas as pd

sys.path.append("../modules/")

from plot import plot_confusion_matrix_a

# %%
from rede import NeuralNet

# %%
X_trein = np.load('../../data/X_train_res_ren_pdf.npy')
y_trein = np.load('../../data/y_train_res_ren_pdf.npy')

X_test = np.load('../../data/X_test_res_ren_pdf.npy')
y_test = np.load('../../data/y_test_res_ren_pdf.npy')

# %%
mlp = NeuralNet()

# %%
eval_set = (X_test, y_test)
mlp.fit(X=X_trein, y=y_trein, eval_set=eval_set) 

# %%
predict = mlp.predict(X_test)

# %%
plot_confusion_matrix_a(y_test,
                        predict,
                        classes=['NS', 'S'],
                        title='Matriz de Confusão')
