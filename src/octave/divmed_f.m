function meds=divmed_f(entradas,ndiv)

meds=[max(entradas)+1e-8; mean(entradas); min(entradas)-1e-8];

while(size(meds,1)<ndiv+1)
    
    for mm=2:size(meds,1)
        ss=find((entradas<=ones(size(entradas,1),1)*meds(mm-1,:))&(entradas>=ones(size(entradas,1),1)*meds(mm,:)));
        entradas1=zeros(size(entradas));
        entradas1(ss)=entradas(ss);
        meds=[meds;sum(entradas1)./sum(abs(sign(entradas1)))];
        clear ss entradas1
    end
    
    meds_aux=sort(meds,'descend');
    clear meds
    meds=meds_aux;
    clear meds_aux
    
end

% pos=find(entradas>meds(2));
% meds(1)=mean(entradas(pos));
% clear pos
% pos=find(entradas<meds(ndiv));
% meds(ndiv+1)=mean(entradas(pos));
% clear pos

end