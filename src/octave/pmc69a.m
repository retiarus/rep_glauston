clear all
clear all
close all
clc


%% rede neural PMC treinada para separar instancias relativas a atividade convectiva forte (S4 >=0.5) das relativas aa atividade convectiva moderada (S4 >=0.2 e <0.5) e desprezivel(S4<0.2)
format long g
more off
page_output_immediately(1)

load "../../data/entradas_stp1.txt"
load "../../data/saidas_stp1.txt"

entradas=entradas_stp1(:,4:size(entradas_stp1,2));
saidas=saidas_stp1;

limiares=[0.2 0.5];
aux=1:size(entradas_stp1,1);
Iteste1=find(entradas_stp1(:,3)==2013);
Iteste2=find(entradas_stp1(:,2)==12&entradas_stp1(:,3)==2012);
Iteste=[Iteste1;Iteste2];
Itrein=aux(find(~ismember(aux,Iteste)))';
clear entradas_stp1 saidas_stp1 aux Iteste1 Iteste2

II1_trein=Itrein(find(saidas(Itrein)<limiares(1)));
II2_trein=Itrein(find(saidas(Itrein)>=limiares(1)&saidas(Itrein)<limiares(2)));
II3_trein=Itrein(saidas(Itrein)>=limiares(2));

II1_teste=Iteste(saidas(Iteste)<limiares(1));
II2_teste=Iteste(find(saidas(Iteste)>=limiares(1)&saidas(Iteste)<limiares(2)));
II3_teste=Iteste(find(saidas(Iteste)>=limiares(2)));

entradas_trein=entradas([II1_trein;II2_trein;II3_trein],:);
IIC1_trein=(1:length(II1_trein)+length(II2_trein))';
IIC2_trein=(length(II1_trein)+length(II2_trein)+1:length(II1_trein)+length(II2_trein)+length(II3_trein))';

entradas_teste=entradas([II1_teste;II2_teste;II3_teste],:);
IIC1_teste=(1:length(II1_teste)+length(II2_teste))';
IIC2_teste=(length(II1_teste)+length(II2_teste)+1:length(II1_teste)+length(II2_teste)+length(II3_teste))';

clear entradas

%----------------------------------------------------------------------------------
ndiv=16;
for j=1:size(entradas_trein,2)
    divemeds(:,:,j)=[divmed_d(entradas_trein(IIC1_trein,j),ndiv) divmed_d(entradas_trein(IIC2_trein,j),ndiv)];
end

% compl1=length(IIC1_trein);
compl2=1*length(IIC1_trein)-length(IIC2_trein);

% aux1=[];
% entradas_aux1=entradas_trein(IIC1_trein,:);
% vv1=randperm(length(IIC1_trein));
% kk=0;
% while(size(aux1,1)<compl1)
% 
%     kk=kk+1;
%     ent=entradas_trein(IIC1_trein(vv1(kk)),:);
% 
%     for j=1:length(ent)
%         psm=find(divemeds(:,1,j)-ent(j)<0);
%         pse=find(entradas_aux1(:,j)<=divemeds(psm(1)-1,1,j)&entradas_aux1(:,j)>=divemeds(psm(1),1,j));
%         amostras=entradas_aux1(pse,j);
%         psa1=find(amostras>=ent(j));
%         psa2=find(amostras<=ent(j));
%         if((length(psa1)/(divemeds(psm(1)-1,1,j)-ent(j)))<(length(psa2)/(ent(j)-divemeds(psm(1),1,j))))
%             amostras1=sort([amostras(psa1);divemeds(psm(1)-1,1,j)]);
%             [xx,pos]=max(diff(amostras1));
%             nent1(j)=mean(amostras1(pos:pos+1));
%             clear amostras1 xx pos
%         else
%             amostras1=sort([amostras(psa2);divemeds(psm(1),1,j)]);
%             [xx,pos]=max(diff(amostras1));
%             nent1(j)=mean(amostras1(pos:pos+1));
%             clear amostras1 xx pos
%         end
%         clear psm pse amostras psa1 psa2
%     end
%     aux1=[aux1;nent1];
%     entradas_aux1=[entradas_aux1;nent1];
%     clear ent nent1
%     for j=1:size(entradas_trein,2)
%         divemeds(:,1,j)=divmed_d(entradas_aux1(:,j),ndiv);
%     end
%     if(kk==length(IIC1_trein))
%         clear vv1
%         vv1=randperm(length(IIC1_trein));
%         kk=0;
%     end
% end

aux2=[];
entradas_aux2=entradas_trein(IIC2_trein,:);
vv2=randperm(length(IIC2_trein));
kk=0;
while(size(aux2,1)<compl2)

    kk=kk+1;
    ent=entradas_trein(IIC2_trein(vv2(kk)),:);

    for j=1:length(ent)
        psm=find(divemeds(:,2,j)-ent(j)<0);
        pse=find(entradas_aux2(:,j)<=divemeds(psm(1)-1,2,j)&entradas_aux2(:,j)>=divemeds(psm(1),2,j));
        amostras=entradas_aux2(pse,j);
        psa1=find(amostras>=ent(j));
        psa2=find(amostras<=ent(j));
        if((length(psa1)/(divemeds(psm(1)-1,2,j)-ent(j)))<(length(psa2)/(ent(j)-divemeds(psm(1),2,j))))
            amostras1=sort([amostras(psa1);divemeds(psm(1)-1,2,j)]);
            [xx,pos]=max(diff(amostras1));
            nent2(j)=mean(amostras1(pos:pos+1));
%             nent2(j)=mean(amostras1);
            clear amostras1 xx pos
        else
            amostras1=sort([amostras(psa2);divemeds(psm(1),2,j)]);
            [xx,pos]=max(diff(amostras1));
            nent2(j)=mean(amostras1(pos:pos+1));
%             nent2(j)=mean(amostras1);
            clear amostras1 xx pos
        end
        clear psm pse amostras psa1 psa2
    end
    aux2=[aux2;nent2];
    entradas_aux2=[entradas_aux2;nent2];
    clear ent nent2
    for j=1:size(entradas_trein,2)
        divemeds(:,2,j)=divmed_d(entradas_aux2(:,j),ndiv);
    end
    if(kk==length(IIC2_trein))
        clear vv2
        vv2=randperm(length(IIC2_trein));
        kk=0;
    end
end

% IIC1_trein=[IIC1_trein;(size(entradas_trein,1)+(1:size(aux1,1)))'];
% entradas_trein=[entradas_trein;aux1];

IIC2_trein=[IIC2_trein;(size(entradas_trein,1)+(1:size(aux2,1)))'];
entradas_trein=[entradas_trein;aux2];

clear aux2 entradas_aux2 vv2

%----------------------------------------------------------------------------------

aux=entradas_trein;
clear entradas_trein;
Max2=0.95;
Min2=0.05;

for j=1:size(aux,2)
    Max1(j)=max(aux(:,j));
    Min1(j)=min(aux(:,j));
    entradas_trein(:,j)=aux(:,j)*((Max2-Min2)/(Max1(j)-Min1(j))) + (Max1(j)*Min2-Max2*Min1(j))/(Max1(j)-Min1(j));
end

aux=entradas_teste;
clear entradas_teste;

for j=1:size(aux,2)
    entradas_teste(:,j)=aux(:,j)*((Max2-Min2)/(Max1(j)-Min1(j))) + (Max1(j)*Min2-Max2*Min1(j))/(Max1(j)-Min1(j));
end
clear aux Max1 Min1

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

printf("Amostra 0\n")
disp(entradas_teste(1, :));

printf("Amostra 1\n")
disp(entradas_teste(34, :));

for j=1:size(entradas_trein,2)
    printf("Variável: %d\n", j)
    
    # amostras de treinamento
    ent1_trein=entradas_trein(IIC1_trein,j);
    ent2_trein=entradas_trein(IIC2_trein,j);
    
    # amostras de teste
    ent1_teste=entradas_teste(IIC1_teste,j);
    ent2_teste=entradas_teste(IIC2_teste,j);
    
    medias1=divmed_f(ent1_trein,16);
    medias2=divmed_f(ent2_trein,16);

    
    expoentes=0.1:0.001:2-0.001;
    
    for i=1:length(expoentes)
        aux1=ent1_trein.*((medias1(10)./ent1_trein).^expoentes(i));
        aux2=ent2_trein.*((medias2(8)./ent2_trein).^expoentes(i));
        res1(i)=std(aux1)/mean(aux1);
        res2(i)=std(aux2)/mean(aux2);
        clear aux1 aux2
    end
    
    [~,pos1]=sort(abs(res1-std(ent1_trein)/(3*mean(ent1_trein))));
    II1x=find(pos1<=length(expoentes)/2);
    ent1a_trein=ent1_trein.*((medias1(10)./ent1_trein).^expoentes(pos1(II1x(1))));
    ent1a_teste=ent1_teste.*((medias1(10)./ent1_teste).^expoentes(pos1(II1x(1))));
    
    [~,pos2]=sort(abs(res2-std(ent2_trein)/(3*mean(ent2_trein))));
    II2x=find(pos2>=length(expoentes)/2);
    ent2a_trein=ent2_trein.*((medias2(8)./ent2_trein).^expoentes(pos2(II2x(1))));
    ent2a_teste=ent2_teste.*((medias2(8)./ent2_teste).^expoentes(pos2(II2x(1))));
    
    s1a=1/(std(ent1a_trein)*sqrt(2*pi));s2a=1/(std(ent2a_trein)*sqrt(2*pi));
        
    y1a=s1a*exp(-0.5*(((ent1a_trein)-medias1(10)).^2)/((std(ent1a_trein))^2));
    y1b=s1a*exp(-0.5*(((ent2a_trein)-medias1(10)).^2)/((std(ent1a_trein))^2));
    y1c=s1a*exp(-0.5*(((ent1a_teste)-medias1(10)).^2)/((std(ent1a_trein))^2));
    y1d=s1a*exp(-0.5*(((ent2a_teste)-medias1(10)).^2)/((std(ent1a_trein))^2));
    
    y2a=s2a*exp(-0.5*(((ent2a_trein)-medias2(8)).^2)/((std(ent2a_trein))^2));
    y2b=s2a*exp(-0.5*(((ent1a_trein)-medias2(8)).^2)/((std(ent2a_trein))^2));
    y2c=s2a*exp(-0.5*(((ent2a_teste)-medias2(8)).^2)/((std(ent2a_trein))^2));
    y2d=s2a*exp(-0.5*(((ent1a_teste)-medias2(8)).^2)/((std(ent2a_trein))^2));
    
    printf("Mean 1: %f, Std 1: %f, s1a: %f, y1a: %f\n", medias1(10), std(ent1a_trein), s1a, y1c(1))
    printf("Mean 2: %f, Std 2: %f, s1b: %f, y2a: %f\n", medias2(8), std(ent2a_trein), s2a, y1c(1))
    
    disp(ent1a_teste(1))
    res = (ent1a_teste(1) - medias1(10));
    disp(res)
    res = res/std(ent1a_trein);
    disp(res)
    res = res*res;
    disp(res)
    res = exp(-res*0.5);
    disp(res)
    
    disp(ent2a_teste(1))
    res = (ent2a_teste(1) - medias1(10));
    disp(res)
    res = res/std(ent2a_trein);
    disp(res)
    res = res*res;
    disp(res)
    res = exp(-res*0.5);
    disp(res)
    
    printf("max de y1a %d\n", max(y1a))
    printf("max de y1b %d\n", max(y1b))
    printf("max de y1c %d\n", max(y1c))
    printf("max de y1d %d\n", max(y1d))
    y1b=y1b/max(y1a);
    y1c=y1c/max(y1a);
    y1d=y1d/max(y1a);
    
    printf("max de y2a %d\n", max(y2a))
    printf("max de y2b %d\n", max(y2b))
    printf("max de y2c %d\n", max(y2c))
    printf("max de y2d %d\n", max(y2d))
    y2b=y2b/max(y2a);
    y2c=y2c/max(y2a);
    y2d=y2d/max(y2a);

    y1a=y1a/max(y1a);
    y2a=y2a/max(y2a);
    
    entradas_trein1(IIC1_trein,j)=y1a;entradas_trein1(IIC2_trein,j)=y1b;
    entradas_trein2(IIC1_trein,j)=y2b;entradas_trein2(IIC2_trein,j)=y2a;
    
    entradas_teste1(IIC1_teste,j)=y1c;entradas_teste1(IIC2_teste,j)=y1d;
    entradas_teste2(IIC1_teste,j)=y2d;entradas_teste2(IIC2_teste,j)=y2c;    
    
    clear ent1_trein ent1_teste medias1 medias2 expoentes res1 res2 pos1 II1x ent1a_trein ent1a_teste pos2 II2x ent2a_trein ent2a_teste y1a y1b y1c y1d y2a y2b y2c y2d
    
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

printf("Amostra 0\n")
disp(entradas_teste1(1, :));
disp(entradas_teste1(34, :));

printf("\n")

printf("Amostra 1\n")
disp(entradas_teste2(1, :));
disp(entradas_teste2(34, :));


ent1_teste=abs(ones(size(entradas_teste1))-entradas_teste1);
ent2_teste=abs(ones(size(entradas_teste2))-entradas_teste2);

[length(IIC1_trein) length(IIC2_trein)]
[length(IIC1_teste) length(IIC2_teste)]

%-------------------------------------------------------------------------------------

epoca=1;
nmaxepocas=1000000;
alfa=0.001;
alfa_final=0.0001;

incl_o=1;
konst_o=1;
incl_s=1;
konst_s=1;

qalt=25;

nno=size(entradas_trein1,2);

pesos_eo1=rand(nno,nno)*0.01;
pesos_eo2=rand(nno,nno)*0.01;
pesos_os1=rand(nno,1)*0.01;
pesos_os2=rand(nno,1)*0.01;

nao_acabou=1;

while(nao_acabou)
    
    vv=randperm(size(entradas_trein1,1));
    
    for i=1:size(entradas_trein1,1)
        
        ent1=abs(ones(1,size(entradas_trein1,2))-entradas_trein1(vv(i),:));
        ent2=abs(ones(1,size(entradas_trein2,2))-entradas_trein2(vv(i),:));
        
        aoceo1=konst_o./(1+exp(-incl_o*((ent1*pesos_eo1))));
        aoceo2=konst_o./(1+exp(-incl_o*((ent2*pesos_eo2))));
        sairn(vv(i),:)=[konst_s./(1+exp(-incl_s*((aoceo1*pesos_os1)))) ...
                        konst_s./(1+exp(-incl_s*((aoceo2*pesos_os2))))];
        
        % -------------------------------------------------------------------------
        
%         if(ismember(vv(i),IIC1_trein)&(sairn(vv(i),1)>sairn(vv(i),2)))
%             erros1(vv(i))=0-sairn(vv(i),1);
%             erros2(vv(i))=1-sairn(vv(i),2);
%         elseif(ismember(vv(i),IIC2_trein)&(sairn(vv(i),2)>sairn(vv(i),1)))
%             erros1(vv(i))=1-sairn(vv(i),1);
%             erros2(vv(i))=0-sairn(vv(i),2);
%         else
%             erros1(vv(i))=0;
%             erros2(vv(i))=0;
%         end

            if(ismember(vv(i),IIC1_trein))
                erros1(vv(i))=0-sairn(vv(i),1);
                erros2(vv(i))=1-sairn(vv(i),2);
            else
                erros1(vv(i))=1-sairn(vv(i),1);
                erros2(vv(i))=0-sairn(vv(i),2);
            end
        
        % -------------------------------------------------------------------------
        
        delta_os1=erros1(vv(i))*(incl_s*sairn(vv(i),1).*(1-sairn(vv(i),1)))*(aoceo1)';
        delta_os2=erros2(vv(i))*(incl_s*sairn(vv(i),2).*(1-sairn(vv(i),2)))*(aoceo2)';
        
        erros_aoceo1=erros1(vv(i))*(incl_s*sairn(vv(i),1).*(1-sairn(vv(i),1)))*(pesos_os1)';
        erros_aoceo2=erros2(vv(i))*(incl_s*sairn(vv(i),2).*(1-sairn(vv(i),2)))*(pesos_os2)';
        
        delta_eo1=(ent1)'*(erros_aoceo1.*(incl_o*aoceo1.*(1-aoceo1)));
        delta_eo2=(ent2)'*(erros_aoceo2.*(incl_o*aoceo2.*(1-aoceo2)));
        
        % ------------------------------------
        
        pesos_os1=pesos_os1+alfa*delta_os1;
        pesos_os2=pesos_os2+alfa*delta_os2;
        pesos_eo1=pesos_eo1+alfa*delta_eo1;
        pesos_eo2=pesos_eo2+alfa*delta_eo2;

        clear ent1 ent2 aoceo1 aoceo2 delta_os1 delta_os2 erros_aoceo1 erros_aoceo2 delta_eo1 delta_eo2
        
    end
    
    clear vv

    % -----------------------------------------------------------------
    
    erros=[erros1 erros2];
    eqm(epoca,:)=mean(mean(erros.^2,2));
    perroz(epoca)=(length(find(sairn(IIC1_trein,1)<sairn(IIC1_trein,2)))+length(find(sairn(IIC2_trein,2)<sairn(IIC2_trein,1))))/size(entradas_trein1,1);
    ppc(epoca,:)=[length(find(sairn(IIC1_trein,1)<sairn(IIC1_trein,2))) length(find(sairn(IIC2_trein,2)<sairn(IIC2_trein,1)))]./[length(IIC1_trein) length(IIC2_trein)];

    if(epoca==2001)
        sairn_final_trein=sairn;
    end
    clear sairn erros1 erros2 erros
    
    % -----------------------------------------------------------------
    aoceo1=konst_o./(1+exp(-incl_o*((ent1_teste*pesos_eo1))));
    aoceo2=konst_o./(1+exp(-incl_o*((ent2_teste*pesos_eo2))));
    sairn=[konst_s./(1+exp(-incl_s*((aoceo1*pesos_os1)))) ...
           konst_s./(1+exp(-incl_s*((aoceo2*pesos_os2))))];
        % -------------------------------------------------------------------------

    perroz_teste(epoca)=(length(find(sairn(IIC1_teste,1)<sairn(IIC1_teste,2)))+length(find(sairn(IIC2_teste,2)<sairn(IIC2_teste,1))))/size(entradas_teste1,1);
    ppc_teste(epoca,:)=[length(find(sairn(IIC1_teste,1)<sairn(IIC1_teste,2))) length(find(sairn(IIC2_teste,2)<sairn(IIC2_teste,1)))]./[length(IIC1_teste) length(IIC2_teste)];

    if(epoca==2001)
        sairn_final_teste=sairn;
    end
    clear ent1 ent2 aoceo1 aoceo2 sairn
    % -------------------------------------------------------------------------

        if(epoca==2001)
            
            zz=1;
            res_trein(:,:,zz)=[length(find(sairn_final_trein(IIC1_trein,1)<sairn_final_trein(IIC1_trein,2))) length(find(sairn_final_trein(IIC1_trein,1)>sairn_final_trein(IIC1_trein,2))); ...
                              length(find(sairn_final_trein(IIC2_trein,2)>sairn_final_trein(IIC2_trein,1))) length(find(sairn_final_trein(IIC2_trein,2)<sairn_final_trein(IIC2_trein,1)))];
            
            res_teste(:,:,zz)=[length(find(sairn_final_teste(IIC1_teste,1)<sairn_final_teste(IIC1_teste,2))) length(find(sairn_final_teste(IIC1_teste,1)>sairn_final_teste(IIC1_teste,2))); ...
                              length(find(sairn_final_teste(IIC2_teste,2)>sairn_final_teste(IIC2_teste,1))) length(find(sairn_final_teste(IIC2_teste,2)<sairn_final_teste(IIC2_teste,1)))];
            
            xii=sum(diag(res_trein(:,:,zz)));
            xd=sum(sum(res_trein(:,:,zz)));
            plc=sum(sum(res_trein(:,:,zz)).*sum(res_trein(:,:,zz)'));
            kappa_acuracia_trein(zz,:)=[(xd*xii-plc)/(xd^2-plc) sum(diag(res_trein(:,:,zz)))/sum(sum(res_trein(:,:,zz)))];
            clear xii xd plc
            
            xii=sum(diag(res_teste(:,:,zz)));
            xd=sum(sum(res_teste(:,:,zz)));
            plc=sum(sum(res_teste(:,:,zz)).*sum(res_teste(:,:,zz)'));
            kappa_acuracia_teste(zz,:)=[(xd*xii-plc)/(xd^2-plc) sum(diag(res_teste(:,:,zz)))/sum(sum(res_teste(:,:,zz)))];
            clear xii xd plc
            
            clc
            zz
            res_trein(:,:,zz)
            kappa_acuracia_trein(zz,:)
            res_teste(:,:,zz)
            kappa_acuracia_teste(zz,:)

        nao_acabou=0;

    else

        if(~mod(epoca,100))

            [epoca alfa]
            [[mean(ppc(epoca-99:epoca-75,:));mean(ppc(epoca-25:epoca,:))],[mean(perroz(epoca-99:epoca-75));mean(perroz(epoca-25:epoca))]]
            [[mean(ppc_teste(epoca-99:epoca-75,:));mean(ppc_teste(epoca-25:epoca,:))],[mean(perroz_teste(epoca-99:epoca-75));mean(perroz_teste(epoca-25:epoca))]]
        end

        if(~mod(epoca,1000))

            posmedfig=(ones(epoca-(2*qalt-1),1)*[1:2*qalt])+([0:epoca-2*qalt]'*ones(1,2*qalt));
            
            aux1=eqm(:,1);
            figure;plot(mean(aux1(posmedfig),2),'.r');grid;
            
            close all
            
            
           clear aux1
            
            figure;plot(mean(perroz(posmedfig),2),'.b');grid;
            
            close all
            

            figure;plot(mean(perroz_teste(posmedfig),2),'.b');grid;
            
            close all
            

            clear posmedfig

        end
    end

    epoca=epoca+1;

    alfa=((alfa_final./alfa).^(1/((nmaxepocas+1)-epoca)))*alfa;

end

% [0.878231644260598         0.915110408175681         0.796296296296296         0.947368421052632
%  0.806747673216132         0.864270941054809         0.731481481481482         0.894736842105263
%  0.641158221302999         0.9358841778697           0.833333333333333         0.894736842105263
%  0.958117890382626         0.986556359875905         0.803418803418803         0.736842105263158
%  0.858886878416264         0.959258667455093         0.762429153439799         0.894736842105263
%  0.927094105480869         0.956566701137539         0.814814814814815         0.578947368421053
%  0.759259259259259         0.842105263157895         0.788004136504654         0.916235780765253
%  0.902792140641158          0.96277145811789         0.825                     0.868421052631579
%  0.821263684494735         0.964005635042121         0.79243359923288         0.855136571271567
%  0.829767206839494         0.936780176428113         0.845415671397867         0.837697923473243
%  0.849742066330186         0.940588272085836         0.799798579938851         0.857593893495198]

% (sum(xxx)-min(xxx))/(size(xxx,1)-1)
% 0.859190254932132         0.942179279724269          0.80609443883773         0.870350627426842
