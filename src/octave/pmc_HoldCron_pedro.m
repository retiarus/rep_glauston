clear all
clear all
close all
clc

%% rede neural PMC treinada para separar instancias indicadoras da ocorr�ncia de atividade convectiva forte (S4 >=0.5) das indicadoras de atividade convectiva n�o forte (S4<0.5)
format long g

load('entradas_trein_NS.txt'); % entradas j� convertidas pelas "novas PDF's NS" e que ser�o utilizadas no treinamento da rede neural
load('entradas_trein_S.txt');  % entradas j� convertidas pelas "novas PDF's S" e que ser�o utilizadas no treinamento da rede neural
load('entradas_teste_NS.txt'); % entradas j� convertidas pelas "novas PDF's NS" e que ser�o utilizadas para testar a rede neural
load('entradas_teste_S.txt');  % entradas j� convertidas pelas "novas PDF's S" e que ser�o utilizadas para testar a rede neural

IIC1_trein=1:1051;
IIC2_trein=1052:2102;
% As bases de treinamento, 'entradas_trein_NS' e 'entradas_trein_S', t�m o mesmo n�mero de linhas (inst�ncias) que � 2102. 
% As 1051 primeiras linhas (IIC1_trein) em entradas_trein_NS correspondem �s inst�ncias NS convertidas por PDF's NS 
% e as 1051 primeiras linhas (IIC1_trein) em entradas_trein_S correspondem �s inst�ncias NS convertidas por PDF's S
% As linhas de 1052 a 2102 (IIC2_trein) em entradas_trein_NS correspondem �s inst�ncias S convertidas por PDF's NS 
% e as linhas de 1052 a 2102 (IIC2_trein) em entradas_trein_S correspondem �s inst�ncias S convertidas por PDF's S

IIC1_teste=1:32;
IIC2_teste=33:51;
% As bases de teste 'entradas_teste_NS' e 'entradas_teste_S' t�m o mesmo n�mero de linhas (inst�ncias) que � 51. 
% As 32 primeiras linhas (IIC1_teste) em entradas_teste_NS correspondem �s inst�ncias NS convertidas por PDF's NS 
% e as 32 primeiras linhas (IIC1_teste) em entradas_teste_S correspondem a inst�ncias NS convertidas por PDF's S
% As linhas de 33 a 51 (IIC2_teste) em entradas_teste_NS correspondem �s inst�ncias S convertidas por PDF's NS 
% e as linhas de 33 a 51 (IIC2_teste) em entradas_trein_S correspondem a inst�ncias S convertidas por PDF's S

%-------------------------------------------------------------------------------------
epoca=1; %inicializa a vari�vel que conta o n�mero de �pocas
nmaxepocas=1000000;% o n�mero m�ximo de �pocas � fixado em 1e6
alfa=0.001; % a taxa de aprendizado � inicializada no valor 1e-3
alfa_final=0.0001; % o valor da taxa de aprendizado (no n�mero m�ximo de �pocas) � fixado em 1e-4

incl_o=1; %valor da inclina��o das fun��es de ativa��o ocultas
konst_o=1;%valor da amplitude das fun��es de ativa��o ocultas
incl_s=1;%valor da inclina��o das fun��es de ativa��o de sa�da
konst_s=1;%valor da amplitude das fun��es de ativa��o de sa�da

nno=size(entradas_trein_NS,2);% n�mero de neur�nios na camada oculta
pesos_eo1=rand(nno,nno)*0.01; % matriz de pesos conectando entradas e camada oculta na subrede superior (NS-like)
pesos_eo2=rand(nno,nno)*0.01; % matriz de pesos conectando entradas e camada oculta na subrede inferior (S-like)
pesos_os1=rand(nno,1)*0.01;   % vetor de pesos conectando camada oculta e saida na subrede superior (NS-like)
pesos_os2=rand(nno,1)*0.01;   % vetor de pesos conectando camada oculta e saida na subrede inferior(S-like)
% veja que todos os pesos s�o inicializados com pequenos valores aleat�rios

nao_acabou=1; % indica se a fase de treinamento acabou (0) ou n�o (1)

while(nao_acabou)
    
    vv=randperm(size(entradas_trein_NS,1)); 
    % vetor onde as posi��es de 1 a 2102 est�o dispostas aleatoriamente; � utilizado para apresentar as inst�ncias 
    % � rede neural em ordem diferente a cada �poca; portanto, antes do in�cio de cada �poca, um novo vv � gerado
    
    for i=1:size(entradas_trein_NS,1)% treinamento em modo estoc�stico; as inst�ncias s�o apresentadas � rede uma a uma
        
        ent1=abs(ones(1,size(entradas_trein_NS,2))-entradas_trein_NS(vv(i),:));
        ent2=abs(ones(1,size(entradas_trein_S,2))-entradas_trein_S(vv(i),:));
        % as diferen�as absolutas entre as inst�ncias 'entradas_trein_NS(vv(i),:)' e 'entradas_trein_S(vv(i),:)' e um
        % vetor de 1's s�o armazenadas nas vari�veis ent1 e ent2,respectivamente
        
        clio1=ent1*pesos_eo1; 
        % calculam-se os campos locais induzidos (ou seja, o sinal na entrada) dos neur�nios ocultos da subrede superior
        clio2=ent2*pesos_eo2;
        % calculam-se os campos locais induzidos (ou seja, o sinal na entrada) dos neur�nios ocultos da subrede inferior
        
        aoceo1=konst_o./(1+exp(-incl_o*((clio1))));
        % calculam-se as ativa��es (ou seja, o sinal na sa�da) dos neur�nios ocultos da subrede superior, com uma fun��o
        % de ativa��o do tipo sigmoide bin�ria: y=b/(1+exp(-a*x)) que varia de 0 a 1
        
        aoceo2=konst_o./(1+exp(-incl_o*((clio2))));
        % calculam-se as ativa��es (ou seja, o sinal na sa�da) dos neur�nios ocultos da subrede inferior, com uma fun��o
        % de ativa��o do tipo sigmoide bin�ria: y=b/(1+exp(-a*x))
        
        clis1=aoceo1*pesos_os1; % campo local induzido do neur�nio de sa�da da subrede superior
        clis2=aoceo2*pesos_os2; % campo local induzido do neur�nio de sa�da da subrede inferior

        sairn_trein(vv(i),:)=[konst_s./(1+exp(-incl_s*((clis1)))) ...
                              konst_s./(1+exp(-incl_s*((clis2))))];
        % as saidas das duas subredes (ou seja, as ativa��es dos neur�nios de sa�da) s�o calculadas avaliando-se os 
        % respectivos campos locais induzidos com fun��es de ativa��o do tipo sigmoide bin�ria: y=b/(1+exp(-a*x))
        % veja que sairn_trein(vv(i),:) tem duas posi��es, sairn_trein(vv(i),1) e sairn_trein(vv(i),2)
        
        % -------------------------------------------------------------------------
        if(ismember(vv(i),IIC1_trein)) % se a instancia pertence � classe NS
            erros1(vv(i))=0-sairn_trein(vv(i),1);
            erros2(vv(i))=1-sairn_trein(vv(i),2);
        else % se a instancia pertence � classe S
            erros1(vv(i))=1-sairn_trein(vv(i),1);
            erros2(vv(i))=0-sairn_trein(vv(i),2);
        end
        % nesta vers�o da rede, optei por gerar um erro a cada apresenta��o de uma inst�ncia de treinamento; 
        % se a instancia pertence � classe NS (ou seja, se a posi��o vv(i) est� entre 1 e 1059) a saida da 
        % subrede superior deve ser menor do que a sa�da da subrede inferior; assim, o valor de refer�ncia para
        % a saida superior � 0(zero) e o valor de refer�ncia para a saida inferior � 1(um) e os erros s�o calculados
        % conforme dado acima; procedimento inverso aplica-se ao caso em que a inst�ncia pertence � classe S
        % -------------------------------------------------------------------------------------------
        % -------------------------------------------------------------------------------------------
        % o ajuste dos pesos no backpropagation � feito segundo a chamada regra delta definida assim:
        % REGRA DELTA: peso(i+1)=peso(i)+alfa*(erro*deriv)*sig_entrada
        % onde: 
        % peso(i+1)� o novo valor do peso
        % peso(i) � o valor atual do peso
        % alfa � a taxa de aprendizado
        % erro � o erro associado � sa�da
        % deriv � a derivada da fun��o de ativa��o no ponto onde foi calculada(o campo local induzido)
        % sig_entrada � o sinal que foi multiplicado pelo peso(i) para gerar o campo local induzido
        % obs: o termo '(erro*deriv)*sig_entrada' � chamado delta, da� o nome da regra
        % Exemplo: no comando delta_os1=erros1(vv(i))*(incl_s*sairn_trein(vv(i),1).*(1-sairn_trein(vv(i),1)))*(aoceo1)';
        % erros1(vv(i)) --> erro;
        % (incl_s*sairn_trein(vv(i),1).*(1-sairn_trein(vv(i),1))) --> deriv
        % (aoceo1)' --> sig_entrada
        %-------------------------------------------------------------------------
        % os comandos que seguem calculam os ajustes (deltas) dos diversos pesos segundo a regra delta
        % O MELHOR A FAZER � ESTUDAR O BACKPROPAGATION EM UM BOM LIVRO TEXTO, COMO AQUELE QUE EU INDIQUEI
        % E PRA FACILITAR, QUANDO FOR IMPLEMENTAR EM PYTON PRA REPRODUZIR RESULTAOS, USE AS MESMAS VARI�VEIS
        % (OS MESMOS NOMES) UTILIZADOS NESSE PROGRAMA
        %-------------------------------------------------------------------------
        delta_os1=erros1(vv(i))*(incl_s*sairn_trein(vv(i),1).*(1-sairn_trein(vv(i),1)))*(aoceo1)';
        delta_os2=erros2(vv(i))*(incl_s*sairn_trein(vv(i),2).*(1-sairn_trein(vv(i),2)))*(aoceo2)';
        
        erros_aoceo1=erros1(vv(i))*(incl_s*sairn_trein(vv(i),1).*(1-sairn_trein(vv(i),1)))*(pesos_os1)';
        erros_aoceo2=erros2(vv(i))*(incl_s*sairn_trein(vv(i),2).*(1-sairn_trein(vv(i),2)))*(pesos_os2)';
        
        delta_eo1=(ent1)'*(erros_aoceo1.*(incl_o*aoceo1.*(1-aoceo1)));
        delta_eo2=(ent2)'*(erros_aoceo2.*(incl_o*aoceo2.*(1-aoceo2)));
        % ------------------------------------
        % aplica��o da regra delta; veja que aqui a taxa de aprendizado � multiplicada pelo delta
        pesos_os1=pesos_os1+alfa*delta_os1;
        pesos_os2=pesos_os2+alfa*delta_os2;
        pesos_eo1=pesos_eo1+alfa*delta_eo1;
        pesos_eo2=pesos_eo2+alfa*delta_eo2;
        
        clear ent1 ent2 clio1 clio2 aoceo1 aoceo2 clis1 clis2 delta_os1 delta_os2 erros_aoceo1 erros_aoceo2 delta_eo1 delta_eo2
        
    end
    
    % -----------------------------------------------------------------
    
    erros=[erros1 erros2];
    eqm(epoca,:)=mean(mean(erros.^2,2)); % c�lculo do erro quadr�tico m�dio a cada �poca; aqui fiz a media dos erros das duas sa�das
    
    clear vv erros1 erros2 erros
    if(mod(epoca,100)~=0&epoca<=2000)
        clear sairn_trein
    end
    
    % -------------------------------------------------------------------------
    
    if(epoca==2001) % embora tenha fixado o n�mero m�ximo de �pocas em 1e6, j� sei que com 2000 �pocas o programa converge e a rede fica treinada
        
        nao_acabou=0; % termino do treinamento
        
        % resultado (final) para as inst�ncias de treinamento
        res_trein=[length(find(sairn_trein(IIC1_trein,1)<sairn_trein(IIC1_trein,2))) length(find(sairn_trein(IIC1_trein,1)>sairn_trein(IIC1_trein,2))); ...
                   length(find(sairn_trein(IIC2_trein,2)>sairn_trein(IIC2_trein,1))) length(find(sairn_trein(IIC2_trein,2)<sairn_trein(IIC2_trein,1)))];
        
        % c�lculo dos indicadores de desempenho,  kappa e acuracia no treinamento
        xii=sum(diag(res_trein));
        xd=sum(sum(res_trein));
        plc=sum(sum(res_trein).*sum(res_trein'));
        kappa_acuracia_trein=[(xd*xii-plc)/(xd^2-plc) sum(diag(res_trein))/sum(sum(res_trein))];
        clear sairn_trein xii xd plc
        res_trein
        kappa_acuracia_trein
        figure;plot(eqm,'.r');grid;
        
        % -----------------------------------------------------------------
        % as inst�ncias de teste s�o aplicados � rede treinada 
        ent1_teste=abs(ones(size(entradas_teste_NS))-entradas_teste_NS);
        ent2_teste=abs(ones(size(entradas_teste_S))-entradas_teste_S);
        aoceo1=konst_o./(1+exp(-incl_o*((ent1_teste*pesos_eo1))));
        aoceo2=konst_o./(1+exp(-incl_o*((ent2_teste*pesos_eo2))));
        sairn_teste=[konst_s./(1+exp(-incl_s*((aoceo1*pesos_os1)))) ...
                     konst_s./(1+exp(-incl_s*((aoceo2*pesos_os2))))];
        % -------------------------------------------------------------------------
        
        % resultado para as inst�ncias de teste
        res_teste=[length(find(sairn_teste(IIC1_teste,1)<sairn_teste(IIC1_teste,2))) length(find(sairn_teste(IIC1_teste,1)>sairn_teste(IIC1_teste,2))); ...
                   length(find(sairn_teste(IIC2_teste,2)>sairn_teste(IIC2_teste,1))) length(find(sairn_teste(IIC2_teste,2)<sairn_teste(IIC2_teste,1)))];
        
        % c�lculo dos indicadores de desempenho,  kappa e acuracia no teste       
        xii=sum(diag(res_teste));
        xd=sum(sum(res_teste));
        plc=sum(sum(res_teste).*sum(res_teste'));
        kappa_acuracia_teste=[(xd*xii-plc)/(xd^2-plc) sum(diag(res_teste))/sum(sum(res_teste))];
        clear sairn_teste xii xd plc
        res_teste
        kappa_acuracia_teste
        
        
    else
        
        if(~mod(epoca,100)) % isso � feito a cada 100 �pocas de treinamento
            
            epoca
            eqm(epoca,:)
            % resultado (parcial) para as inst�ncias de treinamento
            res_trein=[length(find(sairn_trein(IIC1_trein,1)<sairn_trein(IIC1_trein,2))) length(find(sairn_trein(IIC1_trein,1)>sairn_trein(IIC1_trein,2))); ...
                       length(find(sairn_trein(IIC2_trein,2)>sairn_trein(IIC2_trein,1))) length(find(sairn_trein(IIC2_trein,2)<sairn_trein(IIC2_trein,1)))];
            
            % c�lculo dos indicadores de desempenho (parciais),  kappa e acuracia no treinamento
            xii=sum(diag(res_trein));
            xd=sum(sum(res_trein));
            plc=sum(sum(res_trein).*sum(res_trein'));
            kappa_acuracia_trein=[(xd*xii-plc)/(xd^2-plc) sum(diag(res_trein))/sum(sum(res_trein))];
            res_trein
            kappa_acuracia_trein
            clear sairn_trein res_trein xii xd plc kappa_acuracia_trein
            
        end
        
        epoca=epoca+1; % incremento da vari�vel �poca
        alfa=((alfa_final./alfa).^(1/((nmaxepocas+1)-epoca)))*alfa; % reajuste no valor de alfa segundo uma curva exponencial
        
    end
end