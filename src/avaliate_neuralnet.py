# -*- coding: utf-8 -*-
# ---
# jupyter:
#   jupytext:
#     formats: ipynb,py:percent
#     text_representation:
#       extension: .py
#       format_name: percent
#       format_version: '1.2'
#       jupytext_version: 1.0.5
#   kernelspec:
#     display_name: Python (dscience)
#     language: python
#     name: dscience
# ---

# %%
import numpy as np
import pandas as pd

from plot import plot_confusion_matrix_a

# %%
from neuralnet import MLPGlaustonKERAS2015

# %%
df_trein_s = pd.read_csv("entradas_trein_S.txt", sep="   ", header=None)
df_trein_ns = pd.read_csv("entradas_trein_NS.txt", sep="   ", header=None)

df_trein_inst_ns_pdf_ns = df_trein_ns[0:1051]
df_trein_inst_ns_pdf_s = df_trein_s[0:1051]

df_trein_inst_s_pdf_ns = df_trein_ns[1051:2102]
df_trein_inst_s_pdf_s = df_trein_s[1051:2102]

X_trein_inst_ns_pdf_ns = df_trein_inst_ns_pdf_ns.values
X_trein_inst_ns_pdf_s = df_trein_inst_ns_pdf_s.values
X_trein_inst_ns = np.concatenate([X_trein_inst_ns_pdf_ns, X_trein_inst_ns_pdf_s], axis=1)

X_trein_inst_s_pdf_ns = df_trein_inst_s_pdf_ns.values
X_trein_inst_s_pdf_s = df_trein_inst_s_pdf_s.values
X_trein_inst_s = np.concatenate([X_trein_inst_s_pdf_ns, X_trein_inst_s_pdf_s], axis=1) 

print("X_trein_inst_ns shape: ", X_trein_inst_ns.shape)
y_trein_inst_ns = np.zeros((X_trein_inst_ns.shape[0],1))

print("X_trein_inst_s: ", X_trein_inst_s.shape)
y_trein_inst_s = np.ones((X_trein_inst_s.shape[0],1))

X_trein = np.concatenate([X_trein_inst_ns, X_trein_inst_s])
y_trein = np.concatenate([y_trein_inst_ns, y_trein_inst_s])
np.save('X_train', X_trein)
np.save('y_train', y_trein)

# %%
df_test_s = pd.read_csv("entradas_teste_S.txt", sep="   ", header=None)
df_test_ns = pd.read_csv("entradas_teste_NS.txt", sep="   ", header=None)

df_test_inst_ns_pdf_ns = df_test_ns[0:32]
df_test_inst_ns_pdf_s = df_test_s[0:32]

df_test_inst_s_pdf_ns = df_test_ns[32:51]
df_test_inst_s_pdf_s = df_test_s[32:51]

X_test_inst_ns_pdf_ns = df_test_inst_ns_pdf_ns.values
X_test_inst_ns_pdf_s = df_test_inst_ns_pdf_s.values
X_test_inst_ns = np.concatenate([X_test_inst_ns_pdf_ns, X_test_inst_ns_pdf_s], axis=1)

X_test_inst_s_pdf_ns = df_test_inst_s_pdf_ns.values
X_test_inst_s_pdf_s = df_test_inst_s_pdf_s.values
X_test_inst_s = np.concatenate([X_test_inst_s_pdf_ns, X_test_inst_s_pdf_s], axis=1) 

print("X_test_inst_ns shape: ", X_test_inst_ns.shape)
y_test_inst_ns = np.zeros((X_test_inst_ns.shape[0],1))

print("X_trein_inst_s: ", X_trein_inst_s.shape)
y_test_inst_s = np.ones((X_test_inst_s.shape[0],1))

X_test = np.concatenate([X_test_inst_ns, X_test_inst_s])
y_test = np.concatenate([y_test_inst_ns, y_test_inst_s])
np.save('X_test', X_test)
np.save('y_test', y_test)

# %%
mlp = MLPGlaustonKERAS2015()

# %%
eval_set = (X_test, y_test)
mlp.fit(X=X_trein, y=y_trein, eval_set=eval_set) 

# %%
predict = mlp.predict(X_test)

# %%
plot_confusion_matrix_a(y_test,
                        predict,
                        classes=['NS', 'S'],
                        title='Matriz de Confusão')
