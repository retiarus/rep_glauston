# -*- coding: utf-8 -*-
# ---
# jupyter:
#   jupytext:
#     formats: ipynb,py:percent
#     text_representation:
#       extension: .py
#       format_name: percent
#       format_version: '1.2'
#       jupytext_version: 1.0.5
#   kernelspec:
#     display_name: Python (dscience)
#     language: python
#     name: dscience
# ---

# %%
import os
import sys
import warnings
import pickle

import pdb

import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
import seaborn as sns

from evolutionary_search import EvolutionaryAlgorithmSearchCV


from sklearn.model_selection import train_test_split
from sklearn.metrics import recall_score, make_scorer, confusion_matrix
from sklearn.preprocessing import StandardScaler, MinMaxScaler
from sklearn.pipeline import Pipeline
from sklearn.utils.multiclass import unique_labels
from sklearn.metrics import roc_auc_score

from xgboost import XGBClassifier
from hyperopt import STATUS_OK, Trials, fmin, hp, tpe

from functools import partial

from tests import train_test_split_intime_resample, train_test_split_kfold_resample
from tests import test_intime, test_kfold, test_kfold_hyperopt

# #%matplotlib inline
warnings.filterwarnings('ignore')

latter_size = 14
plt.rcParams['legend.fontsize'] = latter_size 
plt.rcParams['font.size'] = latter_size 
plt.rcParams['axes.labelsize'] = latter_size
plt.rcParams['xtick.labelsize'] = latter_size
plt.rcParams['ytick.labelsize'] = latter_size

# %%
df_train = pd.read_pickle('data_train.pkl')
df_test = pd.read_pickle('data_test.pkl')

# %%
target = ['discretize_s4', ]
predictors = [i for i in df_train.columns if i not in target]
score_function = roc_auc_score

# %% [markdown]
# ## Teste báisco com validação cruzada

# %%
# simple Kfold cross validation
mod = XGBClassifier(objective="binary:hinge", metric="logloss")

test_kfold(df_train, 
           df_test, 
           train_test_split_kfold_resample, 
           mod, score_function, 
           predictors, 
           target)

# %% [markdown]
# ## Teste básico com separação no tempo

# %%
mod = XGBClassifier(objective="binary:hinge", metric="logloss")

test_intime(df_train,
            df_test,
            train_test_split_intime_resample,
            mod,
            score_function,
            predictors,
            target)


# %% [markdown]
# # Otimização de hiper-paramêtros

# %%
def _generate_space_hyperopt(space):
    space_hyperopt = {}
    for i in space:
        space_hyperopt[i] = hp.choice(i, space[i])
        
    return space_hyperopt


# %%
score = partial(test_kfold_hyperopt,
                df_train=df_train,
                df_test=df_test,
                split=train_test_split_kfold_resample,
                mod=XGBClassifier,
                score_function=score_function,
                predictors=predictors,
                target=target)


# %%
def optimize(score, space, config, random_state=42, max_evals=1000):
    """
    This is the optimization function that given a space (space here) of 
    hyperparameters and a scoring function (score here), finds the best hyperparameters.
    """
    # To learn more about XGBoost parameters, head to this page: 
    # https://github.com/dmlc/xgboost/blob/master/doc/parameter.md
    
    # Use the fmin function from Hyperopt to find the best hyperparameters
    space_plus_config = {**space, **config}
    best = fmin(score,
                space_plus_config,
                algo=tpe.suggest, 
                # trials=trials, 
                max_evals=max_evals)
    
    return best


# %%
space = {'n_estimators': [int(x) for x in np.linspace(start=100, stop=3000, num=15)],
         'learning_rate': [0.0001, 0.0005, 0.001, 0.005, 0.01, 0.05, 0.1, 0.2, 0.3],
         'max_depth':  np.arange(5, 20, dtype=int),
         'min_child_weight': np.arange(0, 20, dtype=int),
         'subsample': [0.3, 0.5, 0.75, 1.0],
         'gamma': [.3, .5, .7, .9, 1.0],
         'colsample_bytree': [.3, .5, .7, .9, 1.0],
         'scale_pos_weight': np.arange(0, 10, dtype=int),
         'colsample_bylevel': [.3, .5, .7, .9, 1.0]
        }

config = {'eval_metric': 'auc',
          'objective': 'binary:logistic',
          # Increase this number if you have more cores. Otherwise, remove it and it will default 
          # to the maxium number. 
          'nthread': 8,
          'booster': 'gbtree',
          'tree_method': 'exact',
          'silent': 1,
          'seed': 42
         }

# %%
space_hyperopt = _generate_space_hyperopt(space)

# %%
best_hyperparams = optimize(score=score,
                            max_evals=100000,
                            space=space_hyperopt,
                            config=config)

# %%
best_values = {}
for i in best_hyperparams:
    best_values[i] = space[i][best_hyperparams[i]]
best_hyperparams = best_values
best_hyperparams = {**best_hyperparams, **config}

pickle_out = open("best_hyperparams_resample.pickle","wb")
pickle.dump(best_hyperparams, pickle_out)
pickle_out.close()

# %%
mod = XGBClassifier(**best_hyperparams)

test_intime(df_train,
            df_test,
            train_test_split_intime_resample,
            mod,
            score_function,
            predictors,
            target)
