import numpy as np
import pandas as pd

from functools import partial


def _generate_subband_rec(X, space, a, b, control):
    mean = np.mean(X)
    mean_id = (a+b)//2

    space[mean_id] = mean
    control[mean_id] = True

    result = True
    for i in control[a:b]:
        result = result and i

    if result:
        return
    else:
        # lower
        _generate_subband_rec(X[df < mean],
                              space,
                              a, mean_id,
                              control)

        # upper
        _generate_subband_rec(X[mean <= df],
                              space,
                              mean_id, b,
                              control)


def _generate_subband(X, step_outer):
    space = np.zeros(step_outer+1, dtype=float)
    control = np.zeros(step_outer+1, dtype=bool)
    space[0] = X.min()
    control[0] = True
    space[-1] = X.max()
    control[0] = True
    a = 0
    b = step_outer

    _generate_subband_rec(X, space, a, b, control)

    return space


def _generate_inner_subband(a_value, b_value, step_inner=16):
    inner_space = np.linspace(a_value, b_value, step_inner+1)
    return inner_space


def _generate_complete_set_subbands(X, step_inner, step_outer):
    size = step_outer*step_inner
    complete_space = np.zeros(size+1, dtype=float)

    # give the values for the outer spce
    space = _generate_subband(X, step_outer)
    for idx in range(0, step_outer+1):
        complete_space[idx*step_inner] = space[idx]

    # complete the space
    for idx in range(0, step_outer):
        idx_a = idx*step_inner
        idx_b = (idx+1)*step_inner
        complete_space[idx_a:idx_b+1] = _generate_inner_subband(space[idx],
                                                                space[idx+1],
                                                                step_inner)

    return complete_space


def _get_subband(value, space):
    size = len(space) - 1
    for i in range(0, size):
        if space[i] <= value <= space[i+1]:
            return i


def _generate_subbands(X, step_inner, step_outer):
    num_columns = X.shape[1]
    subbands = {}
    for i in np.arange(num_columns):
        space = _generate_complete_set_subbands(X[:, i],
                                                step_inner,
                                                step_outer)
        aux = {}
        aux['space'] = space
        aux['space_count'] = np.zeros(len(space)-1, dtype=np.int)
        subbands[i] = aux

    return subbands


def _generate_representaion_populate_subbands(X, subbands):
    num_columns = X.shape[1]
    num_rows = X.shape[0]
    X_subband_representation = []
    for i in np.arange(num_rows):
        subband_representation = {}
        for variable in np.arange(num_columns):
            index = _get_subband(value=X[i, j],
                                 space=subbands[variable]['space'])
            subband_representation[variable] = index
            subbands[variable]['space_count'][index] += 1
        df_subband_representation.append(subband_representation)

    return X_subband_representation


def _get_inner(idx, step_inner):
    return idx % step_inner


def _get_outer(idx, step_inner):
    return idx // step_inner


def _generate_sample(ori_sample, step_inner, subbands):
    num_columns = ori_sample.shape[0]
    new_sample = {}
    for i in np.arange(num_columns):
        idx_outer = _get_outer(ori_sample[i], step_inner)
        idx_inner = _get_outer(ori_sample[i], step_inner)

        a = idx_outer*step_inner
        b = (idx_outer+1)*step_inner

        # id_min is the possition of the region less populated in the inner block
        id_min = subbands[i]['space_count'][a:b].argmin()
        id_min_global = a + id_min

        new_sample[i] =  (subbands[i]['space'][id_min_global]
                          + subbands[i]['space'][id_min_global+1])/2.

    return new_sample

def resample_df(X, step_inner, step_outer, size_out):
    size_in = X.shape[0]
    while size_in < size_out:
        subbands = _generate_subbands(X,
                                      step_inner=step_inner,
                                      step_outer=step_outer)
        subband_representation_X = _generate_representaion_populate_subbands(X, subbands)

        new_samples = []
        for i in subband_representation_X:
            nsample = _generate_sample(i, step_inner, subbands)
            new_samples.append(nsample)
            if len(new_samples) + size_in >= size_out:
                break

        X_aux = np.array(new_samples)
        X = np.concatenate([X, X_aux], axis=1)
        size_in += len(new_samples)

    return df
