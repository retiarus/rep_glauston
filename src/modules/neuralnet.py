import keras
import sys

import numpy as np
import matplotlib.pyplot as plt
import tensorflow as tf
import keras.backend as K

from keras.layers import Input, Dense
from keras.models import Model
from sklearn.metrics import precision_score
from sklearn.metrics import make_scorer

precision = make_scorer(precision_score, average='macro')


class MLPGlaustonKERAS2015():
    def __init__(self, n_epoches=2000, batch_size=10, verbose=1):
        self._n_epoches = n_epoches
        self._batch_size = batch_size
        self._verbose = verbose

        # subnet1
        input_a = Input(shape=(13,), dtype='float32', name='input_a')
        layer_h1_a = Dense(units=13,
                           activation='sigmoid',
                           kernel_initializer='uniform',
                           use_bias=False,
                           name='layer_h1_a')(input_a)
        layer_h2_a = Dense(units=1, activation='sigmoid',
                           kernel_initializer='uniform',
                           use_bias=False,
                           name='layer_h2_a')(layer_h1_a)
        self._output_a = layer_h2_a

        # subnet2
        input_b = Input(shape=(13,), dtype='float32', name='input_b')
        layer_h1_b = Dense(units=13,
                           activation='sigmoid',
                           kernel_initializer='uniform',
                           use_bias=False,
                           name='layer_h1_b')(input_b)
        layer_h2_b = Dense(units=1,
                           activation='sigmoid',
                           kernel_initializer='uniform',
                           use_bias=False,
                           name='layer_h2_b')(layer_h1_b)
        self._output_b = layer_h2_b

        self._model_a = Model(input_a,
                              outputs=self._output_a)

        self._model_b = Model(input_b,
                              outputs=self._output_b)

        self._model_a.compile(loss='mean_squared_error',
                              optimizer='adam',
                              metrics=['accuracy'])

        self._model_b.compile(loss='mean_squared_error',
                              optimizer='adam',
                              metrics=['accuracy'])

    def fit(self, X, y, eval_set=None):
        X_aux = X - 1.
        X = np.abs(X_aux)

        if eval_set is not None:
            X_test = eval_set[0]
            y_test = eval_set[1]
            X_aux = X_test - 1.
            X_test = np.abs(X_aux)
            X_test_a = X_test[:, 0:13]
            X_test_b = X_test[:, 13:26]
            y_test_a = y_test
            y_test_b = 1 - y_test
            validation_data_a = (X_test_a, y_test_a)
            validation_data_b = (X_test_b, y_test_b)
        else:
            validation_data_a = None
            validation_data_b = None

        # fit the model
        history_a = self._model_a.fit(X[::, 0:13],
                                      y,
                                      validation_data=validation_data_a,
                                      epochs=self._n_epoches,
                                      batch_size=self._batch_size,
                                      verbose=0)

        history_b = self._model_b.fit(X[::, 13:26],
                                      1-y,
                                      epochs=self._n_epoches,
                                      validation_data=validation_data_b,
                                      batch_size=self._batch_size,
                                      verbose=0)

        if self._verbose == 1:
            fig, ax = plt.subplots(nrows=1, ncols=2, figsize=(20, 10))
            # summarize history for precision
            ax[0].plot(history_a.history['acc'], label='train_a')
            ax[0].plot(history_b.history['acc'], label='train_b')
            if eval_set is not None:
                ax[0].plot(history_a.history['val_acc'], label='test_a')
                ax[0].plot(history_b.history['val_acc'], label='test_b')
            ax[0].set_title('model acc')
            ax[0].set_ylabel('precision')
            ax[0].set_xlabel('epoches')
            ax[0].legend(loc='upper left')

            # summarize history for loss
            ax[1].plot(history_a.history['loss'], label='train_a')
            ax[1].plot(history_b.history['loss'], label='train_b')
            if eval_set is not None:
                ax[0].plot(history_a.history['val_loss'], label='test_a')
                ax[0].plot(history_b.history['val_loss'], label='test_b')
            ax[1].set_title('model loss')
            ax[1].set_ylabel('loss')
            ax[1].set_xlabel('epoches')
            ax[1].legend(loc='upper left')

            fig.tight_layout()

    def predict(self, X):
        X_aux = X - 1.
        X = np.abs(X_aux)
        predict_a = self._model_a.predict(X[::, 0:13])
        predict_b = self._model_b.predict(X[::, 13:26])
        predict = predict_a - predict_b
        return np.where(predict > 0.0, 1, 0)

    def transform(self, X):
        return self.predict(X)

    def precision(self, y_true, y_pred):
        """Precision metric.

        Only computes a batch-wise average of precision.
        Computes the precision, a metric for multi-label classification of
        how many selected items are relevant.
        """
        true_positives = K.sum(K.round(K.clip(y_true * y_pred, 0, 1)))
        predicted_positives = K.sum(K.round(K.clip(y_pred, 0, 1)))
        precision = true_positives / (predicted_positives + K.epsilon())
        return precision


class MLPGlauston2015():
    def __init__(self,
                 batch_size,
                 n_epoches,
                 shuffle=True,
                 learning_rate=0.001,
                 verbose=0):

        self._n_input = 26
        self._n_output = 1
        self._learning_rate = learning_rate
        self._X = tf.placeholder(tf.float32, [None, self._n_input])
        self._y = tf.placeholder(tf.float32, [None, self._n_output])
        self._batch_size = batch_size
        self._n_epoches = n_epoches
        self._shuffle = shuffle
        self._verbose = verbose

        weight = {
            # subnet 1
            'h1_a': tf.Variable(tf.random_uniform([13, 13]),
                                dtype=tf.float32),
            'h2_a': tf.Variable(tf.random_uniform([13, 1]),
                                dtype=tf.float32),
            # subnet 2
            'h1_b': tf.Variable(tf.random_uniform([13, 13]),
                                dtype=tf.float32),
            'h2_b': tf.Variable(tf.random_uniform([13, 1]),
                                dtype=tf.float32)
        }

        bias = {
            # subnet 1
            'h1_a': tf.Variable(tf.random_uniform([13]),
                                dtype=tf.float32),
            'h2_a': tf.Variable(tf.random_uniform([1]),
                                dtype=tf.float32),
            # subnet 2
            'h1_b': tf.Variable(tf.random_uniform([13]),
                                dtype=tf.float32),
            'h2_b': tf.Variable(tf.random_uniform([1]),
                                dtype=tf.float32)
        }

        # create model (neural network)
        # subnet 1
        X_a = self._X[::, 0:13]
        h1_a = tf.add(tf.matmul(X_a, weight['h1_a']), bias['h1_a'])
        h1_a = tf.nn.sigmoid(h1_a)
        h2_a = tf.add(tf.matmul(h1_a, weight['h2_a']), bias['h2_a'])
        h2_a = tf.nn.sigmoid(h2_a)

        # subnet 2
        X_b = self._X[::, 13:26]
        h1_b = tf.add(tf.matmul(X_b, weight['h1_b']), bias['h1_b'])
        h1_b = tf.nn.sigmoid(h1_b)
        h2_b = tf.add(tf.matmul(h1_b, weight['h2_b']), bias['h2_b'])
        h2_b = tf.nn.sigmoid(h2_b)

        h3 = h2_b - h2_a
        self._output = h3

        # define loss and optimizer
        self._loss_operation = tf.reduce_mean(tf.square(self._y
                                                        - self._output))
        optimizer = tf.train.AdamOptimizer(learning_rate=learning_rate)
        self._train_operation = optimizer.minimize(self._loss_operation)

        init = tf.global_variables_initializer()
        self._sess = tf.Session()
        self._sess.run(init)

    def fit(self, X, y):
        # preprossing input X
        X_aux = X - 1.
        X = np.abs(X_aux)

        # convert labels from 0, 1 to -1, 1
        y_aux = y.copy()
        y_aux[y_aux == 0] = -1
        y = y_aux

        size_data = X.shape[0]

        # create copy from the train data
        X_copy = X.copy()
        y_copy = y.copy()
        y_copy = y_copy.reshape((-1, 1))

        if self._batch_size > 0:
            n_batches = size_data // self._batch_size \
                        + (0 if size_data % self._batch_size == 0 else 1)
        else:
            n_batches = 1

        if self._shuffle:
            indexes = np.arange(size_data)

        for e in range(self._n_epoches):

            if self._shuffle:
                np.random.shuffle(indexes)
                X_copy = X_copy[indexes]
                y_copy = y_copy[indexes]

            loss = 0.0
            pre = 0.0
            for i in range(n_batches):
                batch_X = X_copy[i*self._batch_size:(i+1)*self._batch_size]
                batch_y = y_copy[i*self._batch_size:(i+1)*self._batch_size]
                self._partial_fit(batch_X, batch_y)

                loss = self._sess.run([self._loss_operation],
                                      feed_dict={self._X: batch_X,
                                                 self._y: batch_y})

    def transform(self, X):
        return self.transform(X)

    def predict(self, X):
        X_aux = X - 1.
        X = np.abs(X_aux)
        predict = self._sess.run(self._output,
                                 feed_dict={self._X: X})
        predict = np.where(predict > 0.0, 1, 0)
        return predict

    def _partial_fit(self, batch_X, batch_y):
        self._sess.run(self._train_operation,
                       feed_dict={self._X: batch_X,
                                  self._y: batch_y})


class SimpleMLPGlauston2015():
    def __init__(self,
                 batch_size,
                 n_epoches,
                 shuffle=True,
                 learning_rate=0.001,
                 verbose=0):

        self._n_input = 13
        self._n_output = 1
        self._learning_rate = learning_rate
        self._X = tf.placeholder(tf.float32, [None, self._n_input])
        self._y = tf.placeholder(tf.float32, [None, self._n_output])
        self._batch_size = batch_size
        self._n_epoches = n_epoches
        self._shuffle = shuffle
        self._verbose = verbose

        weight = {
            'h1': tf.Variable(tf.random_uniform([13, 13]),
                              dtype=tf.float32),
            'h2': tf.Variable(tf.random_uniform([13, 1]),
                              dtype=tf.float32)
        }

        bias = {
            'h1': tf.Variable(tf.random_uniform([13]),
                              dtype=tf.float32),
            'h2': tf.Variable(tf.random_uniform([1]),
                              dtype=tf.float32)
        }

        # create model (neural network)
        h1 = tf.add(tf.matmul(self._X, weight['h1']), bias['h1'])
        h1 = tf.nn.sigmoid(h1)
        h2 = tf.add(tf.matmul(h1, weight['h2']), bias['h2'])
        self._output = tf.nn.sigmoid(h2)

        # convert output to label indexes
        self._predict_value = tf.round(self._output)

        # define loss and optimizer
        self._loss_operation = tf.reduce_mean(
            tf.nn.sigmoid_cross_entropy_with_logits(logits=self._output,
                                                    labels=self._y)
                                        )
        optimizer = tf.train.AdamOptimizer(learning_rate=learning_rate)
        self._train_operation = optimizer.minimize(self._loss_operation)

        init = tf.global_variables_initializer()
        self._sess = tf.Session()
        self._sess.run(init)

    def fit(self, X, y):
        X_aux = X - 1.
        X = np.abs(X_aux)
        size_data = X.shape[0]

        X_copy = X.copy()
        y_copy = y.copy()
        y_copy = y_copy.reshape((-1, 1))

        if self._batch_size > 0:
            n_batches = size_data // self._batch_size \
                        + (0 if size_data % self._batch_size == 0 else 1)
        else:
            n_batches = 1

        if self._shuffle:
            indexes = np.arange(size_data)

        for e in range(self._n_epoches):

            if self._shuffle:
                np.random.shuffle(indexes)
                X_copy = X_copy[indexes]
                y_copy = y_copy[indexes]

            loss = 0.0
            acc = 0.0
            for i in range(n_batches):
                batch_X = X_copy[i*self._batch_size:(i+1)*self._batch_size]
                batch_y = y_copy[i*self._batch_size:(i+1)*self._batch_size]
                self._partial_fit(batch_X, batch_y)

                loss, y_pred = self._sess.run([self._loss_operation,
                                               self._predict_value],
                                              feed_dict={self._X: batch_X,
                                                         self._y: batch_y})

                pre = precision_score(y_true=batch_y,
                                      y_pred=y_pred)

            if self._verbose == 1:
                if e % 100 == 0:
                    print("Epoch " + str(e) + ", Minibatch Loss = " +
                          "{:.4f}".format(loss) + ", Training precission = " +
                          "{:.3f}".format(pre))

    def transform(self, X):
        X_aux = X - 1.
        X = np.abs(X_aux)
        return self._sess.run(self._predict_value,
                              feed_dict={self._X: X})

    def predict(self, X):
        return self.transform(X)

    def _partial_fit(self, batch_X, batch_y):
        self._sess.run(self._train_operation,
                       feed_dict={self._X: batch_X,
                                  self._y: batch_y})
