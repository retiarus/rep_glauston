import sys

import numpy as np
import pandas as pd

import _resample as rs

np.set_printoptions(threshold=sys.maxsize)
np.set_printoptions(linewidth=220)

df_train = pd.read_pickle('../../data/data_train.pkl')

target = 'discretize_s4'
predictors = [i for i in df_train.columns if i not in target]

for j in predictors:
    space = rs.generate_complete_set_subbands(df_train[j].values, 2, 2)
    print(j, space)
print("\n")

X_train = df_train[predictors].values
y_train = df_train[target].values

subbands = rs.generate_subbands(X_train, 2, 2)
print(type(subbands))
print(type(subbands['space2d']))
print(type(subbands['space2d_count']))
print(subbands['space2d'])
print(subbands['space2d_count'])

X_subband_representation = rs.generate_representation_populate_subbands(X_train, subbands)
print(X_subband_representation[0,:], "\n")

X = rs.generate_sample(X_subband_representation[0,:], 2, subbands)
print(X, "\n")

X_new = rs.resample(df_train[predictors].values, 2, 2, 1500)
print(X_new.shape)
print(X_new)

resample = rs.Resample(2, 2)
X_end, y_end = resample.fit_resample(X_train, y_train)
print(X_end.shape)
print(y_end.shape)
