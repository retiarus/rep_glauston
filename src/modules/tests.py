import pdb

import numpy as np
import matplotlib.pyplot as plt
import pandas as pd

from sklearn.model_selection import StratifiedKFold
from sklearn.preprocessing import MinMaxScaler
from sklearn.metrics import confusion_matrix
from hyperopt import STATUS_OK
from imblearn.over_sampling import ADASYN, SMOTE

from plot import plot_confusion_matrix, plot_confusion_matrix_a
from resample import resample_df
from pdf_representation import ToPdfRepresentation


##########################################################################
##########################################################################


def test_kfold(df_train, df_test,
               mod, score_function, target, params=None,
               resample=None,
               early_stopping_rounds=None,
               verbose=1):
    opredictors = [i for i in df_train.columns if i != target]
    kf = StratifiedKFold(n_splits=10, shuffle=True)
    conf_matrix = np.zeros((2, 2))
    scores = []
    losses = []
    step = 1
    for train_index, test_index in kf.split(df_train[opredictors].values,
                                            df_train[target].values):

        # select train data from df_train
        X_train = df_train[opredictors].values[train_index]
        y_train = df_train[target].values[train_index].ravel()

        # select test data from df_train
        X_test = df_train[opredictors].values[test_index]
        y_test = df_train[target].values[test_index].ravel()

        if resample is not None:
            X_train, y_train = resample.fit_resample(X_train, y_train)

        # give the number of samples in each class
        if verbose == 1:
            print(f"Número de amostras da classe 1.0: {y_train[y_train == 1.0].shape[0]}")
            print(f"Número de amostras da classe 0.0: {y_train[y_train == 0.0].shape[0]}")

        # generate standardize transformation for (x,y)
        X_scaler = MinMaxScaler(feature_range=(0.05, 0.95))
        X_scaler.fit(X_train)

        # transform the the data
        X_train = X_scaler.transform(X_train)
        X_test = X_scaler.transform(X_test)

        # fit the model
        if early_stopping_rounds:
            eval_set = [(X_train, y_train), (X_test, y_test)]
            mod.fit(X_train, y_train, eval_set=eval_set,
                    early_stopping_rounds=early_stopping_rounds,
                    verbose=False)
        else:
            mod.fit(X_train, y_train)

        # use the final model to avaliate the error
        predict = mod.predict(X_test)
        conf_matrix += confusion_matrix(y_test, predict, labels=[0, 1])

        score = score_function(y_true=y_test,
                               y_pred=predict)
        scores.append(score)
        if verbose == 1:
            print("Fold: {:02d}, Score: {:2f}, Loss: {:2f}".format(step,
                                                                   score,
                                                                   1. - score))

        step += 1

    conf_matrix /= 10.0
    plot_confusion_matrix(cm=conf_matrix,
                          classes=['NS', 'S'],
                          use_float=True,
                          title='Matriz de Confusão')

    if verbose == 1:
        scores = np.array(scores)
        losses = 1 - scores
        print("Score: {:2f} {:2f}, Loss: {:2f} {:2f}".format(np.mean(scores),
                                                             np.std(scores),
                                                             np.mean(losses),
                                                             np.std(losses)))


def test_intime(df_train, df_test,
               mod, score_function, target, params=None,
               resample=None,
               early_stopping_rounds=None,
               verbose=1):
    opredictors = [i for i in df_train.columns if i != target]

    # select train data from df_train
    X_train = df_train[opredictors].values
    y_train = df_train[target].values

    # select test data from df_train
    X_test = df_test[opredictors].values
    y_test = df_test[target].values

    if resample is not None:
        X_train, y_train = resample.fit_resample(X_train, y_train)

    # give the number of samples in each class
    if verbose == 1:
        print("Número de amostras da classe 1.0: {}".format(y_train[y_train == 1.0].shape[0]))
        print("Número de amostras da classe 0.0: {}".format(y_train[y_train == 0.0].shape[0]))

    # transform (scaler) the train and test data
    X_scaler = MinMaxScaler(feature_range=(0.05, 0.95))
    X_scaler.fit(X_train)
    X_train = X_scaler.transform(X_train)
    X_test = X_scaler.transform(X_test)

    # fit the model
    if early_stopping_rounds:
        eval_set = [(X_train, y_train), (X_test, y_test)]
        mod.fit(X_train, y_train, eval_set=eval_set,
                eval_metric=["error", "logloss"],
                early_stopping_rounds=early_stopping_rounds)

        if verbose == 1:
            # retrieve performance metrics
            results = mod.evals_result()
            epochs = len(results['validation_0']['error'])
            x_axis = range(0, epochs)

            # plot log loss and classification error
            fig, ax = plt.subplots(1, 2)
            ax[0].plot(x_axis,
                        results['validation_0']['logloss'], label='Train')
            ax[0].plot(x_axis,
                       results['validation_1']['logloss'], label='Test')
            ax[0].legend()
            plt.ylabel('Loss')
            plt.title('Loss')

            ax[1].plot(x_axis,
                       results['validation_0']['error'], label='Train')
            ax[1].plot(x_axis,
                       results['validation_1']['error'], label='Test')
            ax[1].legend()
            plt.ylabel('Classification Error')
            plt.title('Classification Error')
            plt.tight_layout()
    else:
        mod.fit(X_train, y_train)

    # use the final model to avaliate the error in a sample of the time series
    predict = mod.predict(X_test)
    conf_matrix = confusion_matrix(y_test, predict, labels=[0, 1])

    plot_confusion_matrix_a(y_test,
                            predict,
                            classes=['NS', 'S'],
                            title='Matriz de Confusão')

    if verbose == 1:
        score = score_function(y_true=y_test,
                               y_pred=predict)
        loss = 1 - score
        print("Score: {:2f}, Loss: {:2f}".format(score, loss))


def test_kfold_hyperopt(params,
                        df_train, df_test,
                        mod, score_function, target,
                        resample=None,
                        early_stopping_rounds=None,
                        verbose=0):
    # num_round = int(params['n_estimators'])
    del params['n_estimators']

    mod = mod(**params)
    # implement Kfold cross validation
    score = 0.0
    opredictors = [i for i in df_train.columns if i != target]
    kf = StratifiedKFold(n_splits=10, shuffle=True)
    for train_index, test_index in kf.split(df_train[opredictors].values,
                                            df_train[target].values):

        # select train data from df_train
        X_train = df_train[opredictors].values[train_index]
        y_train = df_train[target].values[train_index].ravel()

        # select test data from df_train
        X_test = df_train[opredictors].values[test_index]
        y_test = df_train[target].values[test_index].ravel()

        if resample is not None:
            X_train, y_train = resample.fit_resample(X_train, y_train)

        # generate standardize transformation for (x,y)
        X_scaler = MinMaxScaler(feature_range=(0.05, 0.95)) # transformation for X
        X_scaler.fit(X_train)

        # transform the the data
        X_train = X_scaler.transform(X_train)
        X_test = X_scaler.transform(X_test)

        if early_stopping_rounds:
            eval_set = [(X_train, y_train), (X_test, y_test)]
            mod.fit(X_train, y_train, eval_set=eval_set,
                    early_stopping_rounds=early_stopping_rounds,
                    verbose=False)
        else:
            mod.fit(X_train, y_train)

        # use the final model to avaliate the error in a sample of the time series
        predict = mod.predict(X_test)
        score += score_function(y_true=y_test,
                                y_pred=predict)

    score /= 10.0
    # The score function should return the loss (1-score)
    # since the optimize function looks for the minimum
    loss = 1 - score

    return {'loss': loss, 'status': STATUS_OK}
