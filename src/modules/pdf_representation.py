import numpy as np
import pandas as pd

from functools import partial

from resample import _generate_subband


def _get_pdf(df, step_outer, shift, verbose=None):
    space = _generate_subband(df, step_outer)
    x = df.values

    # mean_idx store the index to the mean value
    # mean_shift will store the value associated
    # with the position mean_idx shiffted by shift variable
    mean_idx = step_outer//2
    mean = space[mean_idx]
    mean_shift = space[mean_idx + shift]

    std = np.std(x)
    cv = std/mean

    # generate differents values to coeficient of variation
    # by using the expression y=x(mean_shift/x)^r
    # the new coefficient will be cv_i = \sigma(y)/\mu(y)
    r_values = np.arange(0.1, 2, 0.001)
    cv_array = np.empty_like(r_values)
    for idx, r in enumerate(r_values):
        y = x*np.power((mean_shift/x), r)
        cv_array[idx] = np.std(y)/np.mean(y)

    vdif = np.abs(cv_array - cv/3)
    position = np.argmin(vdif[0:951])
    y_new = x*np.power((mean_shift/x), r_values[position])
    mean_new = np.mean(y_new)
    std_new = np.std(y_new)
    
    if verbose:
        print(mean_shift, mean_new, std_new)
    def gaussian(x, mu, sig):
        return (1/(sig*np.sqrt(2*np.pi)))*np.exp(-np.power(x - mu, 2.)/(2*np.power(sig, 2.)))

    return partial(gaussian, mu=mean_shift, sig=std_new)


def _generate_pdfs(df, step_outer, shift, target):
    df_0 = df.loc[df[target] == 0.0]
    df_1 = df.loc[df[target] == 1.0]

    predictors = [i for i in df.columns if i != target]
    # a gaussin distribution function will be generated
    # for each element in predictors and for each class,
    # considering a two classes problem

    # class 0
    pdf_set_0 = {}
    for variable in predictors:
        # print(variable, ": ", end="")
        pdf_set_0[variable] = _get_pdf(df_0[variable], step_outer, -np.abs(shift))

    # class 1
    pdf_set_1 = {}
    for variable in predictors:
        # print(variable, ": ", end="")
        pdf_set_1[variable] = _get_pdf(df_1[variable], step_outer, np.abs(shift))

    return pdf_set_0, pdf_set_1


def generate_pdf_representation(df, step_outer, shift, target):
    pdf_set_0, pdf_set_1 = _generate_pdfs(df, step_outer, shift, target)

    df_aux = pd.DataFrame(index=df.index.values)
    for i in pdf_set_0:
        df_aux[i+"_0"] = df[i].apply(pdf_set_0[i])

    for i in pdf_set_1:
        df_aux[i+"_1"] = df[i].apply(pdf_set_1[i])

    df_aux[target] = df[target]

    return df_aux

class ToPdfRepresentation():
    def __init__(self, num_intervals=16, shift=1, target=None):
        self._num_intervals = num_intervals
        self._shift = shift
        self._target = target

    def fit(self, df):
        self._pdf_set_0, self._pdf_set_1 = _generate_pdfs(df,
                                                          self._num_intervals,
                                                          self._shift,
                                                          self._target)

    def transform(self, df):
        df_aux = pd.DataFrame(index=df.index.values)
        for i in self._pdf_set_0:
            df_aux[i+"_0"] = df[i].apply(self._pdf_set_0[i])

        for i in self._pdf_set_1:
            df_aux[i+"_1"] = df[i].apply(self._pdf_set_1[i])

        df_aux[self._target] = df[self._target]

        return df_aux

    def fit_transform(self, df):
        self.fit(df)
        return self.transform(df)
