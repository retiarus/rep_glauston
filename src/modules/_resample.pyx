cimport cython
cimport numpy as np
import numpy as np

from cpython cimport bool

@cython.boundscheck(False) # turn off bounds-checking for entire function
@cython.wraparound(False)  # turn off negative index wrapping for entire function
cdef void _generate_subband_rec(np.ndarray[np.float_t, ndim=1] X,
                                np.ndarray[np.float_t, ndim=1] space,
                                int a,
                                int b,
                                np.ndarray[np.int_t, ndim=1] control):
    cdef np.float_t mean = np.mean(X)
    cdef np.int_t mean_id = (a+b)//2

    space[mean_id] = mean
    control[mean_id] = True

    cdef np.int_t result = 1
    cdef np.int_t i
    for i in control[a:b]:
      if i == 0:
          result = 0

    if result == 1:
        return
    else:
        # lower
        _generate_subband_rec(X[X < mean], space,
                              a, mean_id,
                              control)

        # upper
        _generate_subband_rec(X[X >= mean], space,
                              mean_id, b,
                              control)

@cython.boundscheck(False) # turn off bounds-checking for entire function
cdef np.ndarray[np.float_t, ndim=1] _generate_subband(np.ndarray[np.float_t, ndim=1] X, int step_outer):
    cdef np.ndarray[np.float_t, ndim=1] space = np.zeros(step_outer+1, dtype=np.float)
    cdef np.ndarray[np.int_t, ndim=1] control = np.zeros(step_outer+1, dtype=np.int)
    space[0] = np.min(X)
    control[0] = True
    space[-1] = np.max(X)
    control[0] = True
    cdef int a = 0
    cdef int b = step_outer

    _generate_subband_rec(X, space, a, b, control)

    return space


def generate_subband(np.ndarray[np.float_t, ndim=1] X, int step_outer):
    return _generate_subband(X, step_outer)


@cython.boundscheck(False) # turn off bounds-checking for entire function
@cython.wraparound(False)  # turn off negative index wrapping for entire function
cdef np.ndarray[np.float_t, ndim=1] _generate_inner_subband(float a_value,
                                                            float b_value,
                                                            int step_inner=16):
    cdef np.ndarray[np.float_t, ndim=1] inner_space = np.linspace(a_value, b_value, step_inner+1, dtype=np.float)
    return inner_space


@cython.boundscheck(False) # turn off bounds-checking for entire function
@cython.wraparound(False)  # turn off negative index wrapping for entire functionfunction
cdef np.ndarray[np.float_t, ndim=1] _generate_complete_set_subbands(np.ndarray[np.float_t, ndim=1] X,
                                                                        int step_inner,
                                                                        int step_outer):
    cdef int size = step_outer*step_inner
    cdef np.ndarray[np.float_t, ndim=1] complete_space = np.zeros(size+1,
                                                                  dtype=np.float)

    # give the values for the outer spce
    cdef np.ndarray[np.float_t, ndim=1] space = _generate_subband(X, step_outer)
    cdef int i = 0
    for i in np.arange(0, step_outer+1):
        complete_space[i*step_inner] = space[i]

    # complete the space
    cdef int j
    cdef int idx_a = 0
    cdef int idx_b = 0
    for j in np.arange(0, step_outer):
        idx_a = j * step_inner
        idx_b = (j+1)*step_inner
        complete_space[idx_a:idx_b+1] = _generate_inner_subband(space[j],
                                                                space[j+1],
                                                                step_inner)

    return complete_space


def generate_complete_set_subbands(np.ndarray[np.float_t, ndim=1] X,
                                   int step_inner,
                                   int step_outer):
    return _generate_complete_set_subbands(X, step_inner, step_outer)


@cython.boundscheck(False) # turn off bounds-checking for entire function
cdef int _get_subband(float value, np.ndarray[np.float_t, ndim=1] space):
    cdef int size = space.shape[-1] - 1
    cdef int i = 0
    for i in range(0, size):
        if space[i] <= value <= space[i+1]:
            return i


@cython.boundscheck(False) # turn off bounds-checking for entire function
@cython.wraparound(False)  # turn off negative index wrapping for entire function
cdef dict _generate_subbands(np.ndarray[np.float_t, ndim=2] X, int step_inner, int step_outer):
    cdef int num_columns = X.shape[1]
    cdef int size = step_outer*step_inner

    cdef np.ndarray[np.float_t, ndim=2] space2d = np.empty((size+1,
                                                            num_columns),
                                                          dtype=np.float)

    cdef np.ndarray[np.int_t, ndim=2] space2d_count = np.zeros((step_inner*step_outer,
                                                                 num_columns),
                                                                dtype=np.int)

    cdef int i = 0
    for i in np.arange(num_columns):
        space2d[:, i] = _generate_complete_set_subbands(X[:, i],
                                                step_inner,
                                                step_outer)

    cdef dict subbands = {}
    subbands['space2d'] = space2d
    subbands['space2d_count'] = space2d_count

    return subbands


def generate_subbands(np.ndarray[np.float_t, ndim=2] X, int step_inner, int step_outer):
    return _generate_subbands(X, step_inner, step_outer)


@cython.boundscheck(False) # turn off bounds-checking for entire function
@cython.wraparound(False)  # turn off negative index wrapping for entire function
cdef np.ndarray[np.int_t, ndim=2] _generate_representation_populate_subbands(np.ndarray[np.float_t, ndim=2] X,
                                                                            dict subbands):
    cdef int num_columns = X.shape[1]
    cdef int num_rows = X.shape[0]

    cdef np.ndarray[np.int_t, ndim=2 ] X_subband_representation = np.empty((num_rows, num_columns),
                                                                            dtype=np.int)
    cdef int i = 0
    cdef int id
    for i in np.arange(num_rows):
        for j in np.arange(num_columns):
            id = _get_subband(X[i, j],
                              space=subbands['space2d'][:, j])
            X_subband_representation[i, j] = id
            subbands['space2d_count'][id][j] += 1

    return X_subband_representation


def generate_representation_populate_subbands(np.ndarray[np.float_t, ndim=2] X,
                                              dict subbands):
    return _generate_representation_populate_subbands(X, subbands)


@cython.boundscheck(False) # turn off bounds-checking for entire function
@cython.wraparound(False)  # turn off negative index wrapping for entire function
cdef int _get_inner(int idx, int step_inner):
    return idx % step_inner


@cython.boundscheck(False) # turn off bounds-checking for entire function
@cython.wraparound(False)  # turn off negative index wrapping for entire function
cdef int _get_outer(int idx, int step_inner):
    return idx // step_inner


@cython.boundscheck(False) # turn off bounds-checking for entire function
@cython.wraparound(False)  # turn off negative index wrapping for entire function
cdef np.ndarray[np.float_t, ndim=1] _generate_sample(np.ndarray[np.int_t, ndim=1] ori_sample,
                     int step_inner,
                     dict subbands):
    cdef int num_columns = ori_sample.shape[0]
    cdef np.ndarray[np.float_t, ndim=1] new_sample = np.empty(num_columns, dtype=np.float)
    cdef int idx_outer = 0
    cdef int idx_inner = 0
    cdef int a = 0
    cdef int b = 0
    cdef int id_min = 0
    for i in np.arange(num_columns):
        idx_outer = _get_outer(ori_sample[i], step_inner)
        idx_inner = _get_outer(ori_sample[i], step_inner)

        a = idx_outer*step_inner
        b = (idx_outer+1)*step_inner

        # id_min is the possition of the region less populated in the inner block
        id_min = subbands['space2d_count'][a:b, i].argmin() + a

        new_sample[i] =  (subbands['space2d'][id_min, i]
                          + subbands['space2d'][id_min+1, i])/2.

    return new_sample


def generate_sample(np.ndarray[np.int_t, ndim=1] ori_sample,
                    int step_inner,
                    dict subbands):
    return _generate_sample(ori_sample, step_inner, subbands)


@cython.boundscheck(False) # turn off bounds-checking for entire function
@cython.wraparound(False)  # turn off negative index wrapping for entire function
cdef np.ndarray[np.float_t, ndim=2] _resample(np.ndarray[np.float_t, ndim=2] X,
                                                 int step_inner,
                                                 int step_outer,
                                                 int size_out):
    cdef int size_in = X.shape[0]
    cdef int idx = 0

    cdef np.ndarray[np.float_t, ndim=2] new_X = np.empty((size_out, X.shape[1]), dtype=np.float)
    new_X[0:size_in,:] = X

    cdef dict subbands = {}
    cdef np.ndarray[np.int_t, ndim=2] subband_representation_X
    while size_in < size_out:
        subbands = _generate_subbands(X[0:size_in],
                                      step_inner=step_inner,
                                      step_outer=step_outer)
        subband_representation_X = _generate_representation_populate_subbands(X, subbands)

        for idx, i in enumerate(subband_representation_X):
            if idx + size_in == size_out:
              break
            else:
              new_X[idx+size_in, :] = _generate_sample(i,
                                                       step_inner,
                                                       subbands)
        size_in += idx

    return new_X


def resample(np.ndarray[np.float_t, ndim=2] X,
             int step_inner,
             int step_outer,
             int size_out):
    return _resample(X, step_inner, step_outer, size_out)


def _select_lines(np.ndarray[np.float_t, ndim=2] X, condition):
    n_col_X = X.shape[1]
    print(n_col_X)
    print(type(condition))
    aux = np.broadcast_to(condition.reshape(condition.shape[0], 1),
                         (condition.shape[0], n_col_X))
    return X[aux].reshape(-1, n_col_X)


class Resample():
    def __init__(self, int step_inner, int step_outer):
       self._step_inner = step_inner
       self._step_outer = step_outer

    def fit_resample(self,
                     np.ndarray[np.float_t, ndim=2] X,
                     np.ndarray[np.float_t, ndim=1] y):
        cdef num_columns = X.shape[1]
        cdef tuple counts
        counts = np.unique(y, return_counts=True)
        cdef int class_0 = counts[0][0]
        cdef int class_1 = counts[0][1]

        cdef int n_class_0 = counts[1][0]
        cdef int n_class_1 = counts[1][1]

        cdef np.ndarray[np.float_t, ndim=2] X_0 = np.empty((n_class_0, num_columns),
                                                           dtype=np.float)
        X_0 = _select_lines(X, y == 0)

        cdef np.ndarray[np.float_t, ndim=2] X_1 = np.empty((n_class_1, num_columns),
                                                           dtype=np.float)
        X_1 = _select_lines(X, y == 1)

        cdef np.ndarray[np.float_t, ndim=2] X_out
        cdef np.ndarray[np.float_t, ndim=2] X_end
        cdef np.ndarray[np.int_t, ndim=1] y_0
        cdef np.ndarray[np.int_t, ndim=1] y_1
        if n_class_0 > n_class_1:
            X_out = _resample(X_1,
                              self._step_inner,
                              self._step_outer,
                              n_class_0)
            y_0 = np.zeros(n_class_0, dtype=np.int)
            y_1 = np.ones(n_class_0, dtype=np.int)
            X_end = np.concatenate([X_0, X_out])
        else:
            X_out = _resample(X_0,
                              self._step_inner,
                              self._step_outer,
                              n_class_1)
            y_0 = np.zeros(n_class_1, dtype=np.int)
            y_1 = np.ones(n_class_1, dtype=np.int)
            X_end = np.concatenate([X_out, X_1])

        cdef np.ndarray[np.int_t, ndim=1] y_end
        y_end = np.concatenate([y_0, y_1])

        return X_end, y_end
