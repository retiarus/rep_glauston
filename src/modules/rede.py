import matplotlib.pyplot as plt
import numpy as np

from tqdm import tqdm


class NeuralNet():
    def __init__(self, verbose=0):
        self._verbose = verbose
        self._nmaxepocas = 1000000
        self._alfa = 0.001
        self._alfa_final = 0.0001

        # valor da inclinação das funções de ativação ocultas
        self._incl_o = 1

        # valor da amplitude das funções de ativação ocultas
        self._konst_o = 1

        # valor da inclinação das funções de ativação de saída
        self._incl_s = 1

        # valor da amplitude das funções de ativação de saída
        self._konst_s = 1

        # número de neurônios na camada oculta
        self._nno = 13

        # matriz de pesos conectando entradas e camada oculta na subrede
        # superior (NS-like)
        self._pesos_eo1 = np.random.rand(self._nno, self._nno) * 0.01

        # matriz de pesos conectando entradas e camada oculta na subrede
        # inferior (S-like)
        self._pesos_eo2 = np.random.rand(self._nno, self._nno) * 0.01

        # matriz de pesos conectando camada oculta e saida na subrede superior
        # (NS-like)
        self._pesos_os1 = np.random.rand(self._nno, 1) * 0.01

        # matriz de pesos conctando camada oculta e saída na subrede inferior
        # (S-like)
        self._pesos_os2 = np.random.rand(self._nno, 1) * 0.01

    def _feedforwad(self, X):
        X = X.reshape((-1, X.shape[-1]))
        ent1 = X[::, 0:13]
        ent2 = X[::, 13:26]
        self._ent1 = np.abs(1 - ent1)
        self._ent2 = np.abs(1 - ent2)

        # calcula-se os acmpos locais induzidos: sinal de entrada dos neurônios
        # ocultos
        # subrede superior
        self._clio1 = np.dot(self._ent1, self._pesos_eo1)
        # subrede inferior
        self._clio2 = np.dot(self._ent2, self._pesos_eo2)

        # calcula-se as ativações (sinal de saída) dos neurônios ocultos da
        # subrede com uma função de ativação do tipo sigmoide binária
        # y=b(1+exp(-a*x)) que varia de 0 a 1.
        # variáveis com 1 indicam subrede superior, com 2 subrede inferior
        self._aoceo1 = self._konst_o / (
            1 + np.exp(-self._incl_o * self._clio1))
        self._aoceo2 = self._konst_o / (
            1 + np.exp(-self._incl_o * self._clio2))

        # campo local induzido do nuerônio de saída da
        # subrede superior
        self._clis1 = np.dot(self._aoceo1, self._pesos_os1)
        self._clis2 = np.dot(self._aoceo2, self._pesos_os2)

        # saída após aplicação da função de ativação
        sairn_trein1 = self._konst_s / (
            1 + np.exp(-self._incl_s * self._clis1, ))
        sairn_trein2 = self._konst_s / (
            1 + np.exp(-self._incl_s * self._clis2, ))

        return sairn_trein1, sairn_trein2

    def _generate_output(self, sairn_trein1, sairn_trein2):
        aux = sairn_trein1 - sairn_trein2
        aux = np.where(aux >= 0.0, 1, 0)
        return aux

    def predict(self, X):
        sairn_trein1, sairn_trein2 = self._feedforwad(X)
        return self._generate_output(sairn_trein1, sairn_trein2)

    def _simple_calc_error(self, y, sairn_trein1, sairn_trein2):
        if y == 0:
            erro1 = 0 - sairn_trein1
            erro2 = 1 - sairn_trein2
            # se a instância peretence a classe S
        else:
            erro1 = 1 - sairn_trein1
            erro2 = 0 - sairn_trein2

        return erro1, erro2

    def _calc_error(self, y, sairn_trein1, sairn_trein2):
        vfunc = np.vectorize(self._simple_calc_error)
        return vfunc(y, sairn_trein1, sairn_trein2)

    def fit(self, X, y, eval_set=None):
        # tamanho da amostra de treinamento
        size_train_data = X.shape[0]

        alfa = self._alfa
        eqm_train = []
        eqm_test = []
        eqmt_train = []
        eqmt_test = []
        for epoca in tqdm(np.arange(0, 2000)):
            # para cada época as instâncias de treinamento são aleatoriamente
            # fornecidas a rede neural
            vv = np.arange(size_train_data)
            np.random.shuffle(vv)

            # saída após treinamento
            sairn_trein1 = np.zeros(size_train_data)
            sairn_trein2 = np.zeros(size_train_data)

            # erro calculado
            erros1 = np.zeros(size_train_data)
            erros2 = np.zeros(size_train_data)
            # treinamento em modo estocástico com as instâncias sendo
            # apresentadas uma a uma à rede
            for i in np.arange(size_train_data):
                sairn_trein1[vv[i]], sairn_trein2[vv[i]] \
                    = self._feedforwad(X[vv[i]])

                # calculo do erros
                # para uma instância NS a saída da subrede superior precisa
                # ser menor que da sub inferior. Neste caso, adota-se
                # como valores de referência
                # 0 para a saída superior e 1 para a saída inferior.
                # se a instância pertence a classe NS
                erros1[vv[i]], erros2[vv[i]] \
                    = self._simple_calc_error(y[vv[i]],
                                              sairn_trein1[vv[i]],
                                              sairn_trein2[vv[i]])

                delta_os1 = erros1[vv[i]] \
                    * self._incl_s*sairn_trein1[vv[i]] \
                    * (1-sairn_trein1[vv[i]]) \
                    * self._aoceo1.T

                delta_os2 = erros2[vv[i]] \
                    * self._incl_s*sairn_trein2[vv[i]] \
                    * (1-sairn_trein2[vv[i]]) \
                    * self._aoceo2.T

                erros_aoceo1 = erros1[vv[i]] \
                    * self._incl_s * sairn_trein1[vv[i]] \
                    * (1-sairn_trein1[vv[i]]) \
                    * self._pesos_os1.T

                erros_aoceo2 = erros2[vv[i]] \
                    * self._incl_s * sairn_trein2[vv[2]] \
                    * (1-sairn_trein2[vv[2]]) \
                    * self._pesos_os2.T

                aux1 = self._incl_o * self._aoceo1 * (1 - self._aoceo1)
                delta_eo1 = self._ent1.T * (erros_aoceo1 * aux1)

                aux2 = self._incl_o * self._aoceo2 * (1 - self._aoceo1)
                delta_eo2 = self._ent1.T * (erros_aoceo2 * aux2)

                # aplicação da regra delta
                self._pesos_os1 += alfa * delta_os1
                self._pesos_os2 += alfa * delta_os2
                self._pesos_eo1 += alfa * delta_eo1
                self._pesos_eo2 += alfa * delta_eo2

            # avalia erro quadrático médio
            #erros = np.stack([erros1, erros2], axis=-1)
            #eqm_ep = np.mean(np.mean(erros**2.0, axis=1))
            #eqm_train.append(eqm_ep)

            eqm_train_ep = self._eqm(X, y)
            eqm_train.append(eqm_train_ep)

            # avalia erro quadrático médio total
            eqmt_train_ep = self._eqmt(X, y)
            eqmt_train.append(eqmt_train_ep)

            if eval_set is not None:
                X_test = eval_set[0]
                y_test = eval_set[1]

                # avalia erro quadrático médio
                eqm_test_ep = self._eqm(X_test, y_test)
                eqm_test.append(eqm_test_ep)

                # avalia erro quadrátio médio total
                eqmt_test_ep = self._eqmt(X_test, y_test)
                eqmt_test.append(eqmt_test_ep)

            if self._verbose == 1:
                print("Erro quadrático médio, na época {:04d}: {:.4f}".format(epoca, eqm_train_ep))
                print("Erro quadrático médio total, na época {:04d}: {:.4f}".format(epoca, eqmt_train_ep))

            # ajuste da variável alfa segundo uma cruva exponencial
            num_epc_res = self._nmaxepocas - epoca + 1
            alfa = (self._alfa_final / alfa)**(1 / num_epc_res) * alfa

        self._plot(eqm_train, eqmt_train, 2000,
                   eqm_test=eqm_test, eqmt_test=eqmt_test)

    def _eqm(self, X, y):
        sairn_test1, sair_test2 = self._feedforwad(X)
        erros1, erros2 = self._calc_error(y, sairn_test1, sair_test2)
        erros = np.stack([erros1, erros2], axis=-1)
        eqm_ep = np.mean(np.mean(erros**2.0, axis=1))
        return eqm_ep

    def _eqmt(self, X, y):
        predict = self.predict(X)
        return np.mean((y-predict)**2.0)/2

    def _plot(self, eqm_train, eqmt_train, num_epoches,
              eqm_test=None, eqmt_test=None):
        fig, ax = plt.subplots(nrows=1, ncols=2, figsize=(20, 10))
        eqm_train = np.array(eqm_train)
        # summarize history for precision
        ax[0].plot(np.arange(num_epoches), eqm_train, label='train')
        if len(eqm_test) != 0:
            ax[0].plot(np.arange(num_epoches), eqm_test, label='test')
        ax[0].set_title('model eqm')
        ax[0].set_ylabel('eqm')
        ax[0].set_xlabel('epoches')
        ax[0].legend(loc='upper left')

        eqmt_train = np.array(eqmt_train)
        eqmt_test = np.array(eqmt_test)
        # summarize history for loss
        ax[1].plot(np.arange(num_epoches), eqmt_train, label='train')
        if len(eqmt_test) != 0:
            ax[1].plot(np.arange(num_epoches), eqmt_test, label='test')
        ax[1].set_title('model eqmt')
        ax[1].set_ylabel('eqmt')
        ax[1].set_xlabel('epoches')
        ax[1].legend(loc='upper left')

        fig.tight_layout()
