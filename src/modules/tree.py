import numpy as np

from sklearn.metrics import precision_score
from xgboost import XGBClassifier

class SuperTree():
    def __init__(self, params):
        self._mod_a = XGBClassifier(**params)
        self._mod_b = XGBClassifier(**params)

    def fit(self, X, y):
        self._mod_a.fit(X[::, 0:13], y)
        self._mod_b.fit(X[::, 13:26], y)

    def predict(self, X):
        predict_a = self._mod_a.predict_proba(X[::, 0:13])
        predict_b = self._mod_b.predict_proba(X[::, 13:26])
        predict = (predict_a + predict_b)/2.0
        return np.where(predict[::, 1] >= 0.5, 1, 0)
