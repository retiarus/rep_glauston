import numpy as np
import matplotlib.pyplot as plt

from functools import partial

from _resample import generate_subband


def _select_lines(X, condition):
    n_col_X = X.shape[-1]
    aux = np.broadcast_to(condition, (condition.shape[0], n_col_X))
    return X[aux].reshape(-1, n_col_X)


class ToPdfRepresentation():
    def __init__(self, num_intervals=16, shift=1, target=None):
        self._num_intervals = num_intervals
        self._shift = shift
        self._target = target

    def fit(self, X, y):
        self._generate_pdfs(X, y,
                            self._num_intervals,
                            self._shift,
                            self._target)

        X_aux_0, X_aux_1 = self._partial_transform(X)
        self._max_rep_1_inst_1 = np.zeros(X.shape[-1], dtype=np.float32)
        self._max_rep_0_inst_0 = np.zeros(X.shape[-1], dtype=np.float32)
        for i in np.arange(X.shape[-1]):
            self._max_rep_1_inst_1[i] = np.max(_select_lines(X_aux_1, y == 1)[:, i])
            self._max_rep_0_inst_0[i] = np.max(_select_lines(X_aux_0, y == 0)[:, i])

        return self._max_rep_0_inst_0, self._max_rep_1_inst_1

    def transform(self, X):
        num_columns = X.shape[-1]
        X_aux_0, X_aux_1 = self._partial_transform(X)

        for i in np.arange(num_columns):
            X_aux_0[:, i] = X_aux_0[:, i]/float(self._max_rep_0_inst_0[i])
            X_aux_1[:, i] = X_aux_1[:, i]/float(self._max_rep_1_inst_1[i])

        return np.concatenate([X_aux_0, X_aux_1], axis=1)

    def fit_transform(self, X, y):
        self.fit(X, y)
        return self.transform(X)

    def _partial_transform(self, X):
        num_columns = X.shape[-1]

        X_aux_0 = np.empty_like(X)
        X_aux_1 = np.empty_like(X)
        for idx in np.arange(num_columns):
            X_aux_0[:, idx] = self._array_cv_0[idx](X[:, idx])
            if idx == 3:
                print(X_aux_0[0, 3])
            X_aux_0[:, idx] = self._array_pdf_0[idx](X_aux_0[:, idx])

            X_aux_1[:, idx] = self._array_cv_1[idx](X[:, idx])
            if idx == 3:
                print(X_aux_1[0, 3])
            X_aux_1[:, idx] = self._array_pdf_1[idx](X_aux_1[:, idx])

        return X_aux_0, X_aux_1

    def _change_cv(self, X, mean, r):
        return X*np.power((mean/np.abs(X)), r)

    def _get_pdf(self, X, step_outer, shift):
        space = generate_subband(X, step_outer)

        # mean_idx store the index to the mean value
        # mean_shift will store the value associated
        # with the position mean_idx shiffted by shift variable
        mean_idx = step_outer//2
        mean = space[mean_idx]
        mean_shift = space[mean_idx + shift]

        std = np.std(X)
        cv = std/mean

        # generate differents values to coeficient of variation
        # by using the expression y=x(mean_shift/x)^r
        # the new coefficient will be cv_i = \sigma(y)/\mu(y)
        r_values = np.arange(0.1, 2, 0.001)
        cv_array = np.empty_like(r_values)
        for idx, r in enumerate(r_values):
            y = self._change_cv(X, mean_shift, r)
            cv_array[idx] = np.std(y)/np.mean(y)

        vdif = np.abs(cv_array - cv/3)
        if shift < 0:
            position = np.argmin(vdif[0:950])
        elif shift > 0:
            position = np.argmin(vdif[950:1900]) + 950

        # print("Valor de r: ", r_values[position])
        y_new = self._change_cv(X, mean_shift, r_values[position])
        std_new = np.std(y_new)
        # print("Mean: %f, Std: %f" % (mean_shift, std_new))

        def gaussian(x, mu, sig):
            return (1/(sig*np.sqrt(2*np.pi))) \
                * np.exp(-np.power(x - mu, 2.) / (2 * np.power(sig, 2.)))

        function_g = partial(gaussian, mu=mean_shift, sig=std_new)
        function_c = partial(self._change_cv,
                             mean=mean_shift,
                             r=r_values[position])

        return function_g, function_c

    def _generate_pdfs(self, X, y, step_outer, shift, target):
        num_columns = X.shape[-1]
        X_0 = _select_lines(X, y == 0)
        X_1 = _select_lines(X, y == 1)

        # a gaussin distribution function will be generated
        # for each element in predictors and for each class,
        # considering a two classes problem

        # class 0
        self._array_pdf_0 = np.empty(num_columns, dtype=object)
        self._array_cv_0 = np.empty(num_columns, dtype=object)
        for i in np.arange(num_columns):
            self._array_pdf_0[i], self._array_cv_0[i] = self._get_pdf(X_0[:, i],
                                                      step_outer,
                                                     -np.abs(shift))

        # class 1
        self._array_pdf_1 = np.empty(num_columns, dtype=object)
        self._array_cv_1 = np.empty(num_columns, dtype=object)
        for i in np.arange(num_columns):
            self._array_pdf_1[i], self._array_cv_1[i] = self._get_pdf(X_1[:, i],
                                                     step_outer,
                                                     np.abs(shift))
