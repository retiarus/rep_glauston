# -*- coding: utf-8 -*-
# ---
# jupyter:
#   jupytext:
#     formats: ipynb,py:percent
#     text_representation:
#       extension: .py
#       format_name: percent
#       format_version: '1.2'
#       jupytext_version: 1.0.5
#   kernelspec:
#     display_name: Python (dscience)
#     language: python
#     name: dscience
# ---

# %%
import os
import sys
import warnings
import pickle

import pdb

import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
import seaborn as sns

from evolutionary_search import EvolutionaryAlgorithmSearchCV


from sklearn.model_selection import train_test_split
from sklearn.metrics import recall_score, make_scorer, confusion_matrix
from sklearn.preprocessing import StandardScaler, MinMaxScaler
from sklearn.pipeline import Pipeline
from sklearn.utils.multiclass import unique_labels
from sklearn.metrics import roc_auc_score
from sklearn.tree import DecisionTreeClassifier

from xgboost import XGBClassifier
from hyperopt import STATUS_OK, Trials, fmin, hp, tpe

from functools import partial

from tests import train_test_split_intime, train_test_split_kfold
from tests import train_test_split_intime_smote, train_test_split_kfold_smote
from tests import train_test_split_intime_resample, train_test_split_kfold_resample
from tests import train_test_split_intime_adasyn, train_test_split_kfold_adasyn

from tests import test_intime, test_kfold
from tests import test_pdf_representation, test_intime_pdf_representation

# #%matplotlib inline
warnings.filterwarnings('ignore')

latter_size = 14
plt.rcParams['legend.fontsize'] = latter_size 
plt.rcParams['font.size'] = latter_size 
plt.rcParams['axes.labelsize'] = latter_size
plt.rcParams['xtick.labelsize'] = latter_size
plt.rcParams['ytick.labelsize'] = latter_size

# %%
df_train = pd.read_pickle('data_train.pkl')
df_test = pd.read_pickle('data_test.pkl')

# %%
target = ['discretize_s4', ]
predictors = [i for i in df_train.columns if i not in target]
score_function = roc_auc_score

# %% [markdown]
# # Sem Oversamplig

# %% [markdown]
# ### Teste básico com validação cruzada

# %%
# simple Kfold cross validation
clf = DecisionTreeClassifier(random_state=0)

test_kfold(df_train, 
           df_test, 
           train_test_split_kfold, 
           clf, score_function, 
           predictors, 
           target)

# %% [markdown]
# ### Teste básico com separação no tempo

# %%
clf = DecisionTreeClassifier(random_state=0)

test_intime(df_train,
            df_test,
            train_test_split_intime,
            clf,
            score_function,
            predictors,
            target)

# %% [markdown]
# # Oversampling Glauston

# %% [markdown]
# ### Teste básico com validação cruzada

# %%
clf = DecisionTreeClassifier(random_state=0)

test_kfold(df_train, 
           df_test, 
           train_test_split_kfold_resample, 
           clf, score_function, 
           predictors, 
           target)

# %% [markdown]
# ### Teste básico com separação no tempo

# %%
clf = DecisionTreeClassifier(random_state=0)

test_intime(df_train,
            df_test,
            train_test_split_intime_resample,
            clf,
            score_function,
            predictors,
            target)

# %% [markdown]
# # Oversampling Smote

# %% [markdown]
# ### Teste básico com validação cruzada

# %%
clf = DecisionTreeClassifier(random_state=0)

test_kfold(df_train, 
           df_test, 
           train_test_split_kfold_smote,
           clf, score_function, 
           predictors, 
           target)

# %% [markdown]
# ### Teste básico com separação no tempo

# %%
clf = DecisionTreeClassifier(random_state=0)

test_intime(df_train,
            df_test,
            train_test_split_intime_smote,
            clf,
            score_function,
            predictors,
            target)

# %% [markdown]
# # Oversampling Adasyn

# %% [markdown]
# ### Teste básico com validação cruzada

# %%
clf = DecisionTreeClassifier(random_state=0)

test_kfold(df_train, 
           df_test, 
           train_test_split_kfold_adasyn, 
           clf, score_function, 
           predictors, 
           target)

# %% [markdown]
# ### Teste básico com separação no tempo

# %%
clf = DecisionTreeClassifier(random_state=0)

test_intime(df_train,
            df_test,
            train_test_split_intime_adasyn,
            clf,
            score_function,
            predictors,
            target)

# %% [markdown]
# # Oversamplig Glauston + PDF representation

# %% [markdown]
# ### Teste básico com validação cruzada

# %%
clf = DecisionTreeClassifier(random_state=0)

test_pdf_representation(df_train, 
           df_test, 
           train_test_split_kfold, 
           clf, score_function, 
           predictors, 
           target)

# %% [markdown]
# ### Teste básico com separação no tempo

# %%
clf = DecisionTreeClassifier(random_state=0)

test_intime_pdf_representation(df_train,
                               df_test,
                               train_test_split_intime,
                               clf,
                               score_function,
                               predictors,
                               target)

# %%
